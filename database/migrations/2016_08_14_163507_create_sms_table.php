<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('generated_key');
            $table->string('cp_key');
            $table->string('message');
            $table->string('phone_number');
            $table->integer('masking_id');
            $table->dateTime('schedule');
            $table->string('source');
            $table->integer('sending_status');
            $table->integer('report_status');
            $table->dateTime('sent_date');
            $table->string('log');  
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sms');
    }
}
