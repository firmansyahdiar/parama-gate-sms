<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Masking extends Model
{

  public $table = 'maskings';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'username',
      'password',
      'masking',
	  'quota',
      'medium_notif',
      'medium_max',
      'medium_interval',
      'medium_sent',
      'critical_notif',
      'critical_max',
      'critical_interval',
      'critical_sent',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'username' => 'string',
      'password' => 'string',
      'masking' => 'string',
	  'quota' => 'integer',
      'medium_notif' => 'integer',
      'critical_notif' => 'integer',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
