<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuRole extends Model
{

  public $table = 'menu_role';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'role_id',
      'menu_id',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'role_id' => 'string',
      'menu_id' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  public function menu()
  {
      return $this->belongsTo('App\Models\Menu');
  }
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
