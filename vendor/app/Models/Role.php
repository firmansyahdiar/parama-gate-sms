<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

  public $table = 'roles';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'name',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'name' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];

  public function menu()
  {
      return $this->belongsToMany('App\Models\Menu');
  }
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}

