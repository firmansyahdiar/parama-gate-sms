<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PrefixNumber extends Model
{

  public $table = 'prefix_numbers';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'prefix_id',
      'number',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'prefix_id' => 'integer',
      'number' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
