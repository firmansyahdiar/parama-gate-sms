<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;


class Admin extends Authenticatable
{
    protected $fillable = [
        'username', 'email', 'password',
    ];
    protected function getDateFormat()
	  {
	    return 'Y-m-d H:i:s';
	  }

}
