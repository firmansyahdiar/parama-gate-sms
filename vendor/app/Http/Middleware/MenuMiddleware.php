<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\MenuRole;
use App\Models\Menu;
use App\Models\Role;
use Auth;

class MenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		// echo Auth::user()->role_id;exit();
	  if(!empty(Auth::user()->role_id))
	  {
	  $menu_role = MenuRole::where('role_id',Auth::user()->role_id)->pluck('menu_id');
	  $menu = Menu::whereIn('id',$menu_role)->orderBy('sort','asc')->get();
	  // print_r($menu);exit();
	  foreach($menu as $data)
	  {
		$parent = Menu::where('id',$data->parent_id)->first();
		if(!empty($parent))
		{
			if(empty($data_menus[$parent->id]))
			{
				$data_menus[$parent->id]['name'] = $parent->name;
				$data_menus[$parent->id]['icon'] = $parent->icon;
			}
			$data_menus[$parent->id]['child'][] 	= $data;
		}	
		else
		{
			if(empty($data_menus[0]))$data_menus[0] = '';
			$data_menus[0]['child'][] 	= $data;
		}
	  }
	  $request->session()->put('menu', $menu);
      $request->session()->put('data_menus', $data_menus);
      $data = $request->session()->get('menu');
      // print_r(Auth::user()->role_id);exit();
      return $next($request);
	  }
	  else
	  {
		return redirect()->to('/login');
	  }
    }
}
