<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Jobs\SendingSms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use Auth;
use App\Models\Masking;
use App\Models\FileHistory;
use App\Models\Sms;
use App\Models\Setting;

class ImportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $datas;

    public function index()
    {
      $setting = Setting::where('slug','shared_folder')->first();
      $baseDir  = $setting->value;
      $inputDir = $baseDir.'/input/';
      $prosesDir = $baseDir.'/proses/';
      if ($handle = opendir($inputDir)) {
          while (false !== ($value = readdir($handle)))
          {
            $extension = pathinfo($value, PATHINFO_EXTENSION);
            if($extension == 'xls' or $extension == 'xlsx')
            {
              $fileName = $value;
              $inputFileDir   = $inputDir.$fileName;
              $prosesFileDir  = $prosesDir.date('YmdHis').$fileName;
              $fileinput['name'] = $fileName;
              FileHistory::Create($fileinput);
              rename ( $inputFileDir , $prosesFileDir );
              Excel::filter('chunk')->load($prosesFileDir)->chunk(25000, function($readers) {
                  // $this->datas = $readers->all();
				  ini_set('max_execution_time', 3600+1000);
				  ini_set('memory_limit', '1024M');
				  ini_set('default_socket_timeout', 900);
				  foreach($readers as $data)
				  {
					$data->email    = 'lukmanfrdas@gmail.com';
					$data->password = 123456;
					$masking = Masking::where('masking',$data->masking_id)->first();

					$json = '{
					  "masking_id"    : "'.@$masking->id.'",
					  "email"         : "'.$data->email.'",
					  "password"      : "'.$data->password.'",
					  "phone_number"  : "'.$data->phone_number.'",
					  "schedule"      : "'.$data->schedule.'",
					  "sending_status": "1",
					  "message"       : "'.$data->message.'"
					}';
					$url = url('/sendings');
					$input = json_decode($json,true);
					$web = Auth::guard('web');	
					if( $web->attempt(['email' => $data->email,'password'=>$data->password]) )
					{
						$input['user_id']       = Auth::user()->id;
					}
					
					// $job = new SendingSms($input);
					// $this->dispatch($job);
				  }
              });

              
            }
			elseif($extension == 'csv')
			{
				$fileName = $value;
				$inputFileDir   = $inputDir.$fileName;
				$prosesFileDir  = $prosesDir.date('YmdHis').$fileName;
				$fileinput['name'] = $fileName;
				FileHistory::Create($fileinput);
				rename ( $inputFileDir , $prosesFileDir );
				$datas = $this->csv_to_array( $prosesFileDir , ',' );
				
				ini_set('max_execution_time', 3600*1000);
				$listMasking = Masking::pluck('id','masking');
				foreach($listMasking as $k=>$v)
				{
					unset($listMasking[$k]);
					$listMasking[strtolower($k)] = $v;
					
				}
				print_r($listMasking);
				foreach( $datas as $data )
				{
					if( $data['Phone Number'] != '' )
					{
						$phone      = $data['Phone Number'];
						$message    = $data['Message'];
						$schedule   = $this->get_date($data['Schedule']);
						$masking    = $data[ 'Masking ID' ];
						$input['masking_id'] = @$listMasking[strtolower($masking)];
						$input['phone_number'] = $phone;
						$input['message'] = $message;
						$input['schedule'] = $schedule;
						$input['sending_status'] = 1;
						print_r($input);
						Sms::create($input);
					}
					

				}
				// Sms::insert($input);
			}
          }
      }
    }
	function csv_to_array($filename='', $delimiter=',')
    {
        $file_handle = fopen($filename, 'r');
        fgets($file_handle); // this skips the csv header line
        $header = array( 'Phone Number' , 'Message' , 'Schedule' , 'Masking ID' );
        while (($row = fgetcsv($file_handle)) !== FALSE) {
            if(empty($header))
                $header = $row;
            else
                $data[] = array_combine($header, $row);
            // print_r($header);
        }
        fclose($file_handle);
        return $data;
    }
    function get_date( $date )
	{ 
		// $date = '2016-06-16 12:00:00';
		if (preg_match ("/^([0-9]{2})-([0-9]{2})-([0-9]{2}) ([0-9]{2})$/", $date, $split))	
		{
			$data = '20'.$split[1].'-'.$split[2].'-'.$split[3].' '.$split[4].':00:00';
		}
		// elseif (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}))$/", $date, $split))	
		// {
			// $data = $split[3].'-'.$split[1].'-'.$split[2].' '.$split[4].':00:00';
		// }
		elseif (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2})$/", $date, $split))	
		{
			$data = $split[1].'-'.$split[2].'-'.$split[3].' '.$split[4].':'.$split[5].':00';
		}
		elseif (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}:[0-9]{2}:[0-9]{2})$/", $date, $split))	
		{
			$data = $date;
		}
		else
		{
			$data = date('Y-m-d H:i:s');
		}
		return $data;
	}
}
