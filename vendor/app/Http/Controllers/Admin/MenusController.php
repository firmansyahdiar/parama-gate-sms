<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Menu;
Use View;
Use Validator;

class MenusController extends Controller
{
    private $page_title  = 'Menus';
    private $controller  = 'Menus';
    private $model       = 'Menu';
    private $view        = 'menus';
    private $location    = 'admin';


    public function __construct()
    {
      $page_title 	= $this->page_title;
      $controller 	= $this->controller;
      $model      	= $this->model;
      $view       	= $this->view;
      $location   	= $this->location;
	  $listMenus  	= Menu::where('route', '=', '')->pluck('name','id');
	  $listMenus[0]	= '-';
      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'view',
              'listMenus',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Menu::all();
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, Menu::$rules);

      if ($validation->passes())
      {
          Menu::create($input);

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Menu::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Menu::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Menu::findOrFail($id);

      $input = $request->all();

      $data->fill($input)->save();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Menu::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
}
