<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Prefix;
use App\Models\PrefixNumber;
Use View;
Use Validator;

class PrefixsController extends Controller
{
    private $page_title  = 'Prefixs';
    private $controller  = 'Prefixs';
    private $model       = 'Prefix';
    private $view        = 'prefixs';
    private $location    = 'admin';


    public function __construct()
    {
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $location   = $this->location;

      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'view',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Prefix::all();
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$input = $request->all();
		$validation = Validator::make($input, Prefix::$rules);

		if ($validation->passes())
		{
			Prefix::create($input);

			return redirect()->route($this->location.'.'.$this->view.'.index');
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    	$data = Prefix::findOrFail($id);
		$prefix_numbers = PrefixNumber::where('prefix_id',$id)->get();
		return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data','prefix_numbers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Prefix::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Prefix::findOrFail($id);

      $input = $request->all();

      $validation = Validator::make($input, Prefix::$rules);

      if ($validation->passes())
      {
          $data->fill($input)->save();

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Prefix::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
	public function prefix_number($id,Request $request)
	{
		$input = $request->all();
		$input['prefix_id'] = $id;
		$validation = Validator::make($input, Prefix::$rules);

		if ($validation->passes())
		{
			PrefixNumber::create($input);
			return redirect()->route($this->location.'.'.$this->view.'.index');
		}
	}
	public function delete_prefix_number($id)
	{
		$data = PrefixNumber::findOrFail($id);

		$data->delete();

		return redirect()->route($this->location.'.'.$this->view.'.index');

	}
}
