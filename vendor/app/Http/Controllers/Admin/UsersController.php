<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\Models\Department;
use App\Models\Masking;
use App\Models\UserMasking;
use App\User;
Use View;
Use Validator;

class UsersController extends Controller
{

    private $page_title  = 'Users';
    private $controller  = 'Users';
    private $model       = 'User';
    private $view        = 'users';
    private $location    = 'admin';


    public function __construct()
    {
      $listRoles        = Role::pluck('name', 'id');
	  $listRoles[0]		= '';
      $listDepartments  = Department::pluck('name', 'id');
      $listMaskings		= Masking::pluck('masking', 'id');
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $location   = $this->location;

      View::share(
            compact(
              'listRoles',
              'listDepartments',
              'listMaskings',
              'page_title',
              'controller',
              'model',
              'view',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = User::paginate(10);
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		
		$input = $request->all();
		$user = User::where('username', $input['username'])->first();
		if( empty($user) )
		{
			if(!empty($input['masking_id']))
			{
				if ( !preg_match('/\s/',$input['username']) )
				{	
					$input['api_token']  = str_random(60);
					$input['password']  = bcrypt($input['password']);
					$user = User::create($input);
					$this->save_user_masking($user->id,$input['masking_id']);
					return redirect()->route($this->location.'.'.$this->view.'.index');
				}
				else
				{ 
					return redirect()->back()->withErrors(['Error', 'Wrong Input, Username']);
				}
			}
			else
			{
				return redirect()->back()->withErrors(['Error', 'Please Input Masking']);
			}
		}
		else
		{
			return redirect()->back()->withErrors(['Error', 'Username Exist, Please use another username']);
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = User::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = User::findOrFail($id);
	  $user_masking = UserMasking::where('user_id',$id)->pluck('id','masking_id');
      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data','user_masking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$data = User::findOrFail($id);

		$input = $request->all();
		  if(!empty($input['masking_id']))
		  {
				
				if ( !preg_match('/\s/',$input['username']) )
				{	
					if(!empty($input['password']))$input['password']  = bcrypt($input['password']);
					if(empty($input['password']))unset($input['password']);
					$user = $data->fill($input)->save();
					$this->save_user_masking($id,$input['masking_id']);
					return redirect()->route($this->location.'.'.$this->view.'.index');
				}
				else
				{ 
					return redirect()->back()->withErrors(['Error', 'Wrong Input, Username']);
				}
		  }
		  else
		  {
			  return redirect()->back()->withErrors(['Error', 'Please Input Masking']);
		  }
		
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = User::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
	public function save_user_masking($user_id, $masking = array())
	{
		UserMasking::where('user_id', $user_id)->delete();
		foreach( $masking  as $data )
		{
			$input['user_id'] 		= $user_id;
			$input['masking_id']	= $data;
			
			UserMasking::create($input);
		}
	}
}
