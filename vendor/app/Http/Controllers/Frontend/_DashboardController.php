<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Sms;
use App\Models\Masking;
use App\User;
Use View;
Use Validator;

class DashboardController extends Controller
{
    private $page_title  = 'DASHBOARD';
    private $location    = 'frontend';


    public function __construct()
    {
      $page_title = $this->page_title;
      $location   = $this->location;

      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'view',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	  $app = Sms::count();
      $appsend = Sms::where('cp_key', '<>', NULL)->Where('phone_number', '<>', NULL)->Where('masking_id', '<>', NULL)->count();
      $appfailed = Sms::where('cp_key', '=', NULL)->orWhere('phone_number', '=', NULL)->orWhere('masking_id', '=', NULL)->count();
	  
	  $prov = Sms::where('report_status', '<>', Null)->count();
      $provsend = Sms::where('report_status', '=', '1')->count();
      $provdeliver = Sms::where('report_status', '=', '2')->count();
      $provfailed = Sms::where('report_status', '=', '3')->count();
	  
      $user = User::count();
      $masking = Masking::all();
	  
      return view($this->location.'.dashboard')
		  
          ->with('app', json_encode($app,JSON_NUMERIC_CHECK))
          ->with('appsend', json_encode($appsend,JSON_NUMERIC_CHECK))
          ->with('appfailed', json_encode($appfailed,JSON_NUMERIC_CHECK))
		  
          ->with('prov', json_encode($prov,JSON_NUMERIC_CHECK))
          ->with('provsend', json_encode($provsend,JSON_NUMERIC_CHECK))
          ->with('provdeliver', json_encode($provdeliver,JSON_NUMERIC_CHECK))
          ->with('provfailed', json_encode($provfailed,JSON_NUMERIC_CHECK))
		  
          ->with('user', json_encode($user,JSON_NUMERIC_CHECK))
		  ->with(compact('masking'))
		  ;
		
      //$datas = Department::all();
      //return view($this->location.'.dashboard')
      //    ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return response()->json([
			'name' => 'Abigail',
			'state' => 'CA'
		]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, Department::$rules);

      if ($validation->passes())
      {
          Department::create($input);

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Department::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Department::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Department::findOrFail($id);

      $input = $request->all();

      $validation = Validator::make($input, Department::$rules);

      if ($validation->passes())
      {
          $data->fill($input)->save();

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Department::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
}
