<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Department;
use App\Models\Sms;
use App\Models\Masking;
use App\Models\Prefix;
use App\Models\UserMasking;
use App\Models\PrefixNumber;
use App\Models\Setting;
use App\User;
Use View;
Use Auth;
Use Validator;
Use DB;

class DashboardController extends Controller
{
    private $page_title  = 'DASHBOARD';
    private $location    = 'frontend';
    private $view        = 'dashboard';
    private $aktif        = 'home';


    public function __construct()
    {
      $page_title = $this->page_title;
      $location   = $this->location;
      $view       = $this->view;
      $aktif      = $this->aktif;

      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'view',
              'aktif',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	  $prefix = Prefix::all();
	  
			//$key = array_add(['name' => 'Desk'], 'price', 100);
		foreach ($prefix as $data) {
			$prefix_number = PrefixNumber::where('prefix_id', '=', $data->id)->count();
			//$key["country"]=$data['name'];
			$val=$prefix_number;
			$key =array(["country" => "Czech Republic", "litres" => 2], ["country" => "Ireland", "litres" => 1]);
			//list($keys, $val) = array_divide(['Country' => $data['name']]);
			//array_push($key, $val);
			//array_push($val, $prefix_number);
		}
			$hasil=$key;
			$prefix=json_encode($hasil,JSON_NUMERIC_CHECK);
	  //$prefix = '[{ country: "Czech Republic", litres: 2}, { country: "Ireland", litres: 1 }]';
				  
	  $user_masking = UserMasking::where('user_id',Auth::user()->id)->pluck('masking_id');
	  $app = Sms::whereIn('masking_id',$user_masking)->count();
	  
	  
	  //$appsend = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))->Where('phone_number', '<>', NULL)->Where('masking_id', '<>', NULL)->get();
      $appsend = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph', '=', Null)
					->where('cp_key', '<>', NULL)
					->Where('phone_number', '<>', NULL)
					->WhereIn('masking_id', $user_masking)->get();
      $appsend = $appsend[0]->hitung;
	  
      $appqueue = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph', '=', Null)
                    ->where('cp_key', '=', NULL)
                    ->Where('phone_number', '<>', NULL)
                    ->WhereIn('masking_id', $user_masking)
                    ->get();
      $appqueue = $appqueue[0]->hitung;

      $appfailed = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph', '=', Null)
					->WhereIn('masking_id', $user_masking)
					->where('cp_key', '=', NULL)
					->orWhere('phone_number', '=', NULL)
					->get();
      $appfailed = $appfailed[0]->hitung;
	  
	  
	  $prov = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph_delivery', '=', Null)
					->where('report_status', '<>', Null)
					->WhereIn('masking_id', $user_masking)
					->get();
      $prov = $prov[0]->hitung;
	  
      $provsend = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph_delivery', '=', Null)
					->where('report_status', '=', '1')
					->WhereIn('masking_id', $user_masking)
					->get();
      $provsend = $provsend[0]->hitung;
	  
      $provdeliver = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph_delivery', '=', Null)
					->where('report_status', '=', '2')
					->WhereIn('masking_id', $user_masking)
					->get();
      $provdeliver = $provdeliver[0]->hitung;
	  
      $provfailed = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
					->where('status_graph_delivery', '=', Null)
					->where('report_status', '=', '3')
					->WhereIn('masking_id', $user_masking)
					->get();
      $provfailed = $provfailed[0]->hitung;
	  
      $user = User::count();
      $masking = Masking::whereIn('id',$user_masking)->get();
	  //print_r(json_encode($hasil,JSON_NUMERIC_CHECK));
	  
	  $a = 0;
	  $delivery_chart = '';
	  foreach($masking as $data)
	  {
		$delivery_chart[$a]['name'] = $data->masking;
		for($i=1;$i<=12;$i++)
		{
			$count_delivery = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
							->where(DB::raw('month(sent_date)'), '=', $i)
							->where('report_status', '=', '2')
							->where('masking_id', '=', $data->id)
							->WhereIn('masking_id', $user_masking)
							->get();
			
			if(empty($count_delivery[0]->hitung)){
				$count_delivery = 0;
			}else{
				$count_delivery = $count_delivery[0]->hitung;
			}
			$count_delivery = intval($count_delivery);
			$delivery_chart[$a]['data'][] = $count_delivery; 
		}
		$a++;
	  }
	  $delivery_chart = json_encode($delivery_chart,true);
	  
	  $a = 0;
	  $failed_chart = '';
	  foreach($masking as $data)
	  {
		$failed_chart[$a]['name'] = $data->masking;
		for($i=1;$i<=12;$i++)
		{
			$count_failed = Sms::select(DB::raw('sum(ceiling(cast(DATALENGTH([message]) as float)/153)) as hitung'))
							->where(DB::raw('month(sent_date)'), '=', $i)
							->where('report_status', '=', '3')
							->where('masking_id', '=', $data->id)
							->WhereIn('masking_id', $user_masking)
							->get();
			
			if(empty($count_failed[0]->hitung)){
				$count_failed = 0;
			}else{
				$count_failed = $count_failed[0]->hitung;
			}
			$count_failed = intval($count_failed);
			$failed_chart[$a]['data'][] = $count_failed; 
		}
		$a++;
	  }
	  $failed_chart = json_encode($failed_chart,true);
	  
	  $select_masking = Masking::findOrFail($user_masking[0]);
	  return view($this->location.'.dashboard')
		  
          ->with('app', json_encode($app,JSON_NUMERIC_CHECK))
          ->with('appsend', json_encode($appsend,JSON_NUMERIC_CHECK))
          ->with('appqueue', json_encode($appqueue,JSON_NUMERIC_CHECK))
          ->with('appfailed', json_encode($appfailed,JSON_NUMERIC_CHECK))
		  
          ->with('prov', json_encode($prov,JSON_NUMERIC_CHECK))
          ->with('provsend', json_encode($provsend,JSON_NUMERIC_CHECK))
          ->with('provdeliver', json_encode($provdeliver,JSON_NUMERIC_CHECK))
          ->with('provfailed', json_encode($provfailed,JSON_NUMERIC_CHECK))
		  
          ->with('user', json_encode($user,JSON_NUMERIC_CHECK))
		  //->with('prefix', json_encode($prefix,JSON_NUMERIC_CHECK))
		  ->with(compact('prefix'))
		  ->with(compact('masking'))
		  ->with(compact('user_masking'))
		  ->with(compact('delivery_chart'))
		  ->with(compact('select_masking'))
		  ->with(compact('failed_chart'))
		  ;
		
      //$datas = Department::all();
      //return view($this->location.'.dashboard')
      //    ->with(compact('datas'));
    }
	
	public function reset_app($masking)
    {
		Sms::where('status_graph', null)->where('masking_id',$masking)->update(['status_graph' => 1]);
		$input['graph_app'] = date('Y-m-d H:i:s');
		Masking::where('id',$masking)->update($input);
		return redirect()->back();
    }
	
	public function reset_delivery($masking)
	{
		Sms::where('status_graph_delivery', null)->where('masking_id',$masking)->update(['status_graph_delivery' => 1]);
		$input['graph_delivery'] = date('Y-m-d H:i:s');
		Masking::where('id',$masking)->update($input);
		return redirect()->back();
	}
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //return response()->json([
		//	'name' => 'Abigail',
		//	'state' => 'CA'
		//]);
		
		
	    $user_masking = UserMasking::where('user_id',Auth::user()->id)->pluck('masking_id');
        $data = Sms::WhereIn('masking_id', $user_masking)->where('status_graph', '<>', '1')->orWhere('status_graph', '=', Null)->update(['status_graph' => 1]);
        
        return redirect('frontend/dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, Department::$rules);

      if ($validation->passes())
      {
          Department::create($input);

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Department::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Department::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Department::findOrFail($id);

      $input = $request->all();

      $validation = Validator::make($input, Department::$rules);

      if ($validation->passes())
      {
          $data->fill($input)->save();

          return redirect()->route($this->location.'.'.$this->view);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Department::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
}
