<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Sms;
use App\Models\Masking;
use App\Models\UserMasking;
use App\User;
Use View;
Use Validator;

class UnsentsController extends Controller
{
    
    private $page_title  = 'Unsents';
    private $controller  = 'Unsents';
    private $model       = 'Unsent';
    private $view        = 'unsents';
    private $location    = 'frontend';
    private $aktif    = 'unsent';


    public function __construct()
    {

      $listMasking  = Masking::pluck('masking','id');
      $listUser     = User::pluck('email','id');
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $location   = $this->location;
      $aktif   = $this->aktif;
	  
      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'view',
              'aktif',
              'location',
              'listMasking'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $message = session('message');
	    $tgl_awal = session('tgl_awal');
	    $tgl_akhir = session('tgl_akhir');
		$datas = Sms::with(['masking','user'])
					->where(['masking_id' => 0])
					->orWhere(['masking_id' => null]);
		if( !empty( $message ) ) $datas = $datas->where('message' , 'like' , '%'.$message.'%');
		if( !empty( $tgl_awal ) and !empty($tgl_akhir) )
		{
			$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
		}			
		$datas = $datas->take(10)->paginate(100);
		return view($this->location.'.'.$this->view.'.index')
			->with(compact('datas','status', 'masking', 'tgl_awal', 'tgl_akhir','message'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		//DATA INPUT
		$input = $request->all();
		$message = $request->input('message');
		$tgl_awal = $request->input('tgl_awal');
		$tgl_akhir = $request->input('tgl_akhir');
		
		//MASUKAN KE SESSION
		$request->session()->put('message', $message);
		$request->session()->put('tgl_awal', $tgl_awal);
		$request->session()->put('tgl_akhir', $tgl_akhir);
		
		$datas = Sms::with(['masking','user'])
					->where(['masking_id' => 0])
					->orWhere(['masking_id' => null]);
		if( !empty( $message ) ) $datas = $datas->where('message' , 'like' , '%'.$message.'%');
		if( !empty( $tgl_awal ) and !empty($tgl_akhir) )
		{
			$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
		}			
		$datas = $datas->take(10)->paginate(100);
					
		return view($this->location.'.'.$this->view.'.index')
			->with(compact('datas','status', 'masking', 'tgl_awal', 'tgl_akhir','message'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Sms::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Sms::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Sms::findOrFail($id);

      $input = $request->all();
      // print_r($input);exit();
      $input['sending_status'] = 1;
      $data->fill($input)->save();

      return redirect()->route($this->location.'.'.$this->view.'.index');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Sms::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
	public function export_xls()
	{
		$message = session('message');
	    $tgl_awal = session('tgl_awal');
	    $tgl_akhir = session('tgl_akhir');
		$datas = Sms::with(['masking','user'])
					->where(['masking_id' => 0])
					->orWhere(['masking_id' => null]);
		if( !empty( $message ) ) $datas = $datas->where('message' , 'like' , '%'.$message.'%');
		if( !empty( $tgl_awal ) and !empty($tgl_akhir) )
		{
			$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
		}			
					
		$datas = $datas->get();
		return view($this->location.'.'.$this->view.'.bigdump')
			->with(compact('datas','status', 'masking', 'tgl_awal', 'tgl_akhir','message'));
	}
	public function clear_session(Request $request)
	{
		$request->session()->put('message', '-');
		$request->session()->put('tgl_awal', '');
		$request->session()->put('tgl_akhir', '');
		
		//print_r(session('user'));
        return redirect($this->location.'/reportmaskings');
	}
}
