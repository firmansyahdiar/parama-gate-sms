
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
        <!-- <th class="uk-width-2-10 uk-text-center">User Name</th> -->
        <th class="uk-width-2-10 uk-text-center">Phone Number</th>
        <th class="uk-width-2-10 uk-text-center">Message</th>
        <th class="uk-width-2-10 uk-text-center">Maskings</th>
        <!-- <th class="uk-width-2-10 uk-text-center">Schedule</th> -->
        <th class="uk-width-2-10 uk-text-center">Send Date</th>
        <th class="uk-width-2-10 uk-text-center">Status</th>
        <th class="uk-width-2-10 uk-text-center">SMS Length</th>
        <!-- <th class="uk-width-2-10 uk-text-center" colspan="2">Actions</th> -->
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
            <!-- <td class="uk-text-center">{{ @$data->user->name }}</td> -->
            <td class="uk-text-center">{{ $data->phone_number }}</td>
            <td class="uk-text-center">
				<?php 
					if (strlen($data->message) >= 50) {
						echo substr($data->message, 0, 50) ." ...";
					} else {
						echo $data->message;
					}
				?>
			</td>
            <td class="uk-text-center">{{ @$data->masking->masking }}</td>
            <!-- <td class="uk-text-center">{{ $data->schedule }}</td> -->
            <td class="uk-text-center">{{ $data->sent_date }}</td>
            <td class="uk-text-center">
				<?php 
					if ($data->report_status == 0){ echo "Queue"; } 
					if ($data->report_status == 1){ echo "Pending"; } 
					if ($data->report_status == 2){ echo "Deliver"; }
					if ($data->report_status == 3){ echo "Failed"; }  
				?>
			</td>
            <td class="uk-text-center">{!! ceil(strlen($data->message)/153) !!}</td>
            <!-- <td class="uk-text-right">
                <a href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                <a href="{!! route($location.'.'.$view.'.show', [$data->id]) !!}"><i class="md-icon material-icons">&#xE88F;</i></a>
            </td>
            <td class="">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                    <button class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td> -->
        </tr>
    @endforeach
    </tbody>
</table>

