<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Report.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<table class="table table-striped">
  <thead>
	<tr>
	  <th>Phone</th>
	  <th>Message</th>
	  <th>Masking ID</th>
	  <th>Sent Date</th>
	  <th>Sent Status</th>
	  <th>SMS Length</th>
	</tr>
  </thead>
  <tbody>
	@foreach ($datas as $data)
	<tr>
		<td>{!! $data->phone_number !!}</td>
		<td>{!! $data->message !!}</td>
		<td>{!! @$data->masking->masking !!}</td>
		<td>{!! $data->sent_date !!}</td>
		<td>{!! $report_status[ $data->report_status ] !!}</td>
		<td>{!! ceil(count($data->message)/153) !!}</td>
	</tr>
	@endforeach
  </tbody>
</table>