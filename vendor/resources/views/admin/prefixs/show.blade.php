@extends($location.'.layouts.layouts')

@section('content')
		
    <div id="page_content">
        <div id="page_content_inner">

            <h4 class="heading_a uk-margin-bottom">{{$page_title}}</h4>
				<div class="md-card uk-margin-medium-bottom">
					<div class="md-card-content">
						<div class="uk-overflow-container">
							{!! Form::open(['url' => $location.'/'.$view.'/prefix_number/'.$data->id]) !!}
							<div class="uk-grid" data-uk-grid-margin>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row">
										{!! Form::label('phone_number', 'Number:') !!}
										{!! Form::text('number', null, ['class' => 'md-input']) !!}
									</div>
								</div>
							</div>
							<div class="uk-text-right">
								<a class="md-btn md-btn-flat uk-modal-close" href="{!! route('admin.'.$view.'.index') !!}"> Cancel</a>
								<button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
							</div>
						{!! Form::close() !!}
						<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
							<thead>
							<tr>
								<th colspan="3"></th>
								<th colspan="2" class="uk-width-2-10 uk-text-center">Action</th>
							</tr>
							<tr>
								<th class="uk-width-2-10 uk-text-center">Number</th>
								<th class="uk-width-2-10 uk-text-center">Create</th>
								<th class="uk-width-2-10 uk-text-center">Update</th>
								<!--<th></th>-->
								<th></th>
							</tr>
							</thead>
							<tbody>
							@foreach ($prefix_numbers as $data)
								<tr>
									<td>{{ $data->number }}</td>
									<td class="uk-text-center">{{ $data->created_at }}</td>
									<td class="uk-text-center">{{ $data->updated_at }}</td>
									<!--<td class="uk-text-right">
										<a href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
										<a href="{!! route($location.'.'.$view.'.show', [$data->id]) !!}"><i class="md-icon material-icons">&#xE88F;</i></a>
									</td>-->
									<td class="">
										{!! Form::open(['route' => [$location.'.'.$view.'.delete_prefix_number', $data->id], 'method' => 'delete']) !!}
											<button class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
										{!! Form::close() !!}
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
						@include($location.'.'.$view.'.show_fields')
						<div class="uk-text-right">
							<a href="{!! route('admin.'.$view.'.index') !!}" class="md-btn md-btn-flat md-btn-flat-primary">Back</a>
						</div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--BUTTON TAMBAH-->
    <div class="md-fab-wrapper">
        <!--<a class="md-fab md-fab-accent" data-uk-modal href="#modal_form" id="recordAdd">-->
        <a class="md-fab md-fab-accent" href="{!! route($location.'.'.$view.'.create') !!}" id="recordAdd">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>
	
@endsection