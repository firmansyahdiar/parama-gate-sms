<!doctype html>
 <html class="lte-ie9" lang="en"> 
<!--[if gt IE 9]><! <html lang="en"> <![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{url('assets')}}/img/logo.png" sizes="16x16">
    <link rel="icon" type="image/png" href="{{url('assets')}}/img/logo.png" sizes="32x32">

    <title>GATE - SMS</title>

    <!-- additional styles for plugins -->
        <!-- weather icons -->
        <link rel="stylesheet" href="{{url('bower_components2')}}/weather-icons/css/weather-icons.min.css" media="all">
        <!-- metrics graphics (charts) -->
        <link rel="stylesheet" href="{{url('bower_components2')}}/metrics-graphics/dist/metricsgraphics.css">
        <!-- chartist -->
        <link rel="stylesheet" href="{{url('bower_components2')}}/chartist/dist/chartist.min.css">

    <!-- uikit -->
    <link rel="stylesheet" href="{{url('bower_components2')}}/uikit/css/uikit.almost-flat.min.css" media="all">

    <!-- flag icons -->
    <link rel="stylesheet" href="{{url('assets')}}/icons/flags/flags.min.css" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="{{url('assets')}}/css/main.min.css" media="all">

    <!-- matchMedia polyfill for testing media queries in JS -->
    <!--[if lte IE 9]>
        <script type="text/javascript" src="{{url('bower_components2')}}/matchMedia/matchMedia.js"></script>
        <script type="text/javascript" src="{{url('bower_components2')}}/matchMedia/matchMedia.addListener.js"></script>
    <![endif]-->

</head>
<body class=" sidebar_main_open sidebar_main_swipe">
    <!-- main header -->

    @include('admin.layouts.header')
    @include('admin.layouts.sidebar')

    @yield('content')

    <!--
    @include('admin.layouts.sidebarright')
    -->

    <!-- google web fonts -->
    <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
    </script>

    <!-- common functions -->
    <script src="{{url('assets')}}/js/common.min.js"></script>
    <!-- uikit functions -->
    <script src="{{url('assets')}}/js/uikit_custom.min.js"></script>
    <!-- altair common functions/helpers -->
    <script src="{{url('assets')}}/js/altair_admin_common.min.js"></script>

    <!-- page specific plugins -->
        <!-- d3 -->
        <script src="{{url('bower_components2')}}/d3/d3.min.js"></script>
        <!-- metrics graphics (charts) -->
        <script src="{{url('bower_components2')}}/metrics-graphics/dist/metricsgraphics.min.js"></script>
        <!-- chartist (charts) -->
        <script src="{{url('bower_components2')}}/chartist/dist/chartist.min.js"></script>
        <!-- maplace (google maps) -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="{{url('bower_components2')}}/maplace.js/src/maplace-0.1.3.js"></script>
        <!-- peity (small charts) -->
        <script src="{{url('bower_components2')}}/peity/jquery.peity.min.js"></script>
        <!-- easy-pie-chart (circular statistics) -->
        <script src="{{url('bower_components2')}}/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <!-- countUp -->
        <script src="{{url('bower_components2')}}/countUp.js/countUp.min.js"></script>
        <!-- handlebars.js -->
        <script src="{{url('bower_components2')}}/handlebars/handlebars.min.js"></script>
        <script src="{{url('assets')}}/js/custom/handlebars_helpers.min.js"></script>
        <!-- CLNDR -->
        <script src="{{url('bower_components2')}}/clndr/src/clndr.js"></script>
        <!-- fitvids -->
        <script src="{{url('bower_components2')}}/fitvids/jquery.fitvids.js"></script>

        <!--  dashbord functions -->
        <script src="{{url('assets')}}/js/pages/dashboard.min.js"></script>

    <script>
        $(function() {
            // enable hires images
            altair_helpers.retina_images();
            // fastClick (touch devices)
            if(Modernizr.touch) {
                FastClick.attach(document.body);
            }
        });
    </script>


    <!-- @include('admin.layouts.sidebarmini') -->

    <script>
        $(function() {
            var $switcher = $('#style_switcher'),
                $switcher_toggle = $('#style_switcher_toggle'),
                $theme_switcher = $('#theme_switcher'),
                $mini_sidebar_toggle = $('#style_sidebar_mini'),
                $boxed_layout_toggle = $('#style_layout_boxed'),
                $body = $('body');


            $switcher_toggle.click(function(e) {
                e.preventDefault();
                $switcher.toggleClass('switcher_active');
            });

            $theme_switcher.children('li').click(function(e) {
                e.preventDefault();
                var $this = $(this),
                    this_theme = $this.attr('data-app-theme');

                $theme_switcher.children('li').removeClass('active_theme');
                $(this).addClass('active_theme');
                $body
                    .removeClass('app_theme_a app_theme_b app_theme_c app_theme_d app_theme_e app_theme_f app_theme_g')
                    .addClass(this_theme);

                if(this_theme == '') {
                    localStorage.removeItem('altair_theme');
                } else {
                    localStorage.setItem("altair_theme", this_theme);
                }

            });

            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });

            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }


        // toggle mini sidebar

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }

            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });


        // toggle boxed layout

            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }

            // toggle mini sidebar
            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });


        });
    </script>

    <script src="{{url('assets')}}/js/chart/highcharts.js"></script>
    <script src="{{url('assets')}}/js/chart/highcharts-3d.js"></script>
	
    <script>
    $(function () {
		
            //$(document).ready(function () {
			//function showGraphL(){
			
				var appsend = {{ $appsend }};
				var appfailed = {{ $appfailed }};
				
				var provsend = {{ $provsend }};
				var provfailed = {{ $provfailed }};
				var provdeliver = {{ $provdeliver }};

			// GRAFIK BY PROVIDER
                  $('#1').highcharts({
                      chart: {
                          plotBackgroundColor: null,
                          plotBorderWidth: null,
                          plotShadow: false,
                          type: 'pie'
                      },
                      title: {
                          text: 'APPS '
                      },
                      tooltip: {
                          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                      },
                      plotOptions: {
                          pie: {
                              allowPointSelect: true,
                              cursor: 'pointer',
                              dataLabels: {
                                  enabled: false
                              },
                              showInLegend: true
                          }
                      },
                      series: [{
                          name: 'Persentase',
                          colorByPoint: true,
                          data: [{
                              name: 'Send ('+appsend+')',
                              y: <?php echo $appsend; ?>
                          }, {
                              name: 'Failed ('+appfailed+')',
                              y: appfailed
                          }]
                      }]
                  });
				  
			//GRAFIK BY APPS
                  $('#2').highcharts({
                      chart: {
                          plotBackgroundColor: null,
                          plotBorderWidth: null,
                          plotShadow: false,
                          type: 'pie'
                      },
                      title: {
                          text: 'Conten Provider '
                      },
                      tooltip: {
                          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                      },
                      plotOptions: {
                          pie: {
                              allowPointSelect: true,
                              cursor: 'pointer',
                              dataLabels: {
                                  enabled: false
                              },
                              showInLegend: true
                          }
                      },
                      series: [{
                          name: 'Persentase',
                          colorByPoint: true,
                          data: [{
                              name: 'Pending ('+provsend+')',
                              y: provsend
                          }, {
                              name: 'Failed ('+provfailed+')',
                              y: provfailed
                          }, {
                              name: 'Deliver ('+provdeliver+')',
                              y: provdeliver
                          }]
                      }]
                  });
            //});
    });

    </script>
	
<script src="{{url('assets')}}/js/chart/amcharts2.js"></script>
<script src="{{url('assets')}}/js/chart/serial.js"></script>

<script src="{{url('assets')}}/js/chart/amcharts.js"></script>
<script src="{{url('assets')}}/js/chart/pie.js"></script>
<script src="{{url('assets')}}/js/chart/export.min.js"></script>
<link rel="stylesheet" href="{{url('assets')}}/js/chart/export.css" type="text/css" media="all" />
<script src="{{url('assets')}}/js/chart/light.js"></script>
<script>

var appsend = {{ $appsend }};
var appqueue = {{ $appqueue }};
var appfailed = {{ $appfailed }};

var provsend = {{ $provsend }};
var provfailed = {{ $provfailed }};
var provdeliver = {{ $provdeliver }};

var chart = AmCharts.makeChart( "grafik", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "country": 'Sent ',
    "value": appsend
  }, {
    "country": 'Queue ',
    "value": appqueue
  }, {
    "country": 'Unsent ',
    "value": appfailed
  } ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 60,
  "depth3D": 25,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 50,
  "export": {
    "enabled": false
  }
} );
var chart = AmCharts.makeChart( "grafik2", {
  "type": "pie",
  "theme": "light",
  "dataProvider": [ {
    "country": 'Pending ',
    "value": provsend
  }, {
    "country": 'Failed ',
    "value": provfailed
  }, {
    "country": 'Deliver ',
    "value": provdeliver
  } ],
  "valueField": "value",
  "titleField": "country",
  "outlineAlpha": 60,
  "depth3D": 25,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 50,
  "export": {
    "enabled": false
  }
} );

var chartData = {!! $prefix !!};
var chart = AmCharts.makeChart( "prefix", {
  "type": "pie",
  "theme": "dark",
  "dataProvider": chartData,
  "valueField": "jumlah",
  "titleField": "nama",
  "outlineAlpha": 60,
  "depth3D": 25,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
  "angle": 50,
  "export": {
    "enabled": false
  }
} );
</script>
<!-- GRAFIK BAR -->
<script src="{{url('assets')}}/js/chart/highcharts.js"></script>
<script>
$(function () {
    $('#grafik_cp_deliver').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} sms</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! $delivery_chart !!}
    });
	
    $('#grafik_cp_failed').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} sms</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! $failed_chart !!}
    });
});
</script>
</body>
</html>
