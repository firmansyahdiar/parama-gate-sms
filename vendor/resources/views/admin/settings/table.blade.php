
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <!-- <th class="uk-width-2-10 uk-text-left">Slug</th> -->
        <th class="uk-width-2-10 uk-text-left">Key</th>
        <th class="uk-width-2-10 uk-text-left">Value</th>
        <th class="uk-width-2-10 uk-text-center">Create</th>
        <th class="uk-width-2-10 uk-text-center">Update</th>
        <th class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
			<!-- <td>{!! $data->slug !!}</td> -->
			<td class="uk-text-center">{!! $data->key !!}</td>
            <td class="uk-text-center">{!! $data->value !!}</td>
            <td class="uk-text-center">{{ $data->created_at }}</td>
            <td class="uk-text-center">{{ $data->updated_at }}</td>
            <td class="uk-width-2-10 uk-text-center">
                <a title="Edit" href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
