<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Report.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<table class="table table-striped">
  <thead>
	<tr>
	  <th>Phone</th>
	  <th>Message</th>
	  <th>Schedule</th>
	  <th>Masking ID</th>
	  <th>Created At</th>
	</tr>
  </thead>
  <tbody>
	@foreach ($datas as $data)
	<tr>
		<td>{!! $data->phone_number !!}</td>
		<td>{!! $data->message !!}</td>
		<td></td>
		<td>{!! $data->masking_id !!}</td>
		<td>{!! $data->created_at !!}</td>
	</tr>
	@endforeach
  </tbody>
</table>
