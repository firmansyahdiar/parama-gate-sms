@extends($location.'.layouts.dashboard')

@section('content')

<script src="{{url('assets')}}/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>
<br><br>
    <div id="page_content">
        <div id="page_content_inner">
          <!-- tasks -->
        <div class="uk-grid uk-grid-width-medium-1-3 uk-sortable sortable-handler" data-uk-grid="{gutter:24}" data-uk-sortable>
            <!-- <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content">
						<h3 class="heading_a" style = "text-align:center">APPS </h3>
                        <div id="grafik" style="height: 400px; widht: 100%;"></div>
                    </div>
                </div>
            </div> -->
            <!-- <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-content" style = "text-align:center">
						<h3 class="heading_a ">CONTENT PROVIDER </h3>
                        <div id="grafik2" style="height: 400px; widht: 100%;"></div>
                    </div>
                </div>
            </div> -->
			<!--
				<div id="grafik" style="height: 200px; widht: 100%;"></div>
				<div id="grafik2" style="height: 200px; widht: 100%;"></div>
			-->
				
					
            <div class="uk-width-medium-1-2" style=" position: absolute; top: 150px;">
                        
                        <div class="md-card">
                            <div class="md-card-toolbar">
                                <div class="md-card-toolbar-actions">
                                    <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
                                    <i class="md-icon material-icons md-card-close">&#xE14C;</i>
									<!-- <div class="md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">
										<i class="material-icons">&#xE8EE;</i>
										<div class="uk-dropdown uk-dropdown-bottom" style="min-width: 200px; top: 32px; left: -168px;">
											<ul class="uk-nav">
												<li><a href="#" onclick="diva()">APPS</a></li>
												<li><a href="#" onclick="divb()">CONTENT PROVIDER</a></li>
											</ul>
										</div>
									</div> -->
                                </div>
								<!-- <h3 id="title1" class="md-card-toolbar-heading-text"> -->
								<h3 class="md-card-toolbar-heading-text">
                                    APPS 
                                </h3>
                            </div>
                            <div class="md-card-content">
								<!-- <div id="view1" style="height: 200px; widht: 100%;"></div> -->
								<div id="grafik" style="height: 200px; widht: 100%;"></div>
								<!--
									<div class="uk-grid" data-uk-grid-margin>
										<div class="uk-width-medium-1-2">
											<div class="uk-form-row">
												Last Reset Counter : 
											</div>
										</div>
										<div class="uk-width-medium-1-2">
											<div class="uk-form-row" align="right">
												<a href="" class="user_action_image" title="Reset">
													<i class="material-icons">&#xE3AE;</i>
												</a>
											</div>
										</div>
									</div>
								-->
							</div>
                        </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <!-- <h3 id="title2" class="md-card-toolbar-heading-text"> -->
                            <h3 class="md-card-toolbar-heading-text">
                                CONTENT PROVIDER
                            </h3>
                    </div>
                    <div class="md-card-content">
						<!-- <div id="view2" style="height: 200px; widht: 100%;"></div> -->
						<div id="grafik2" style="height: 200px; widht: 100%;"></div>
						<!--
									<div class="uk-grid" data-uk-grid-margin>
										<div class="uk-width-medium-1-2">
											<div class="uk-form-row">
												Last Reset Counter : 
											</div>
										</div>
										<div class="uk-width-medium-1-2">
											<div class="uk-form-row" align="right">
												<a href="" class="user_action_image" title="Reset">
													<i class="material-icons">&#xE3AE;</i>
												</a>
											</div>
										</div>
									</div>
						-->
					</div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <h3 class="md-card-toolbar-heading-text">
                                CONTENT PROVIDER - DELIVER
                            </h3>
                    </div>
                    <div class="md-card-content">
						<div id="grafik_cp_deliver" style="height: 200px; widht: 100%;"></div>
					</div>
                </div>
            </div>
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <h3 class="md-card-toolbar-heading-text">
                                CONTENT PROVIDER - FAILED
                            </h3>
                    </div>
                    <div class="md-card-content">
						<div id="grafik_cp_failed" style="height: 200px; widht: 100%;"></div>
					</div>
                </div>
            </div>
            <div class="uk-width-medium-1-1">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <h3 class="md-card-toolbar-heading-text">
                                BALANCE
                            </h3>
                    </div>
                    <div class="md-card-content">
						<div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-4 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
						@foreach ($masking as $data)
						<?php
						$url = url('/api/sendings/get_balance/'. $data->masking);
						$json = @file_get_contents($url);
						$balance = json_decode($json, true);
						?>
						<div>
							<div class="md-card">
								<div class="md-card-content" style="Background: #DCDCDC;">
									<div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
									<span class="uk-text-muted uk-text-small"><font color="#000000;"> {{ strtoupper($data->masking) }} </font></span>
									<h2 class="uk-margin-remove">&nbsp
										<?php 
											if (($data->quota*$data->critical_notif)/100 >= $balance) {
												$color="color:red;";
												$kedip="berkedip";
											}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
												$color="color:blue;";
												$kedip="berkedip";
											}else{
												$color="";
												$kedip="";
											}
										?> 
										<span class="countUpMe {{ $kedip }}" style="{{ $color }}">
											 {{ $balance }} 
										</span>
									</h2>
								</div>
							</div>
						</div>
						@endforeach
					</div>
					</div>
                </div>
            </div>
			<!-- 
			<div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <h3 class="md-card-toolbar-heading-text">
                                
                            </h3>
                    </div>
                
                    <div class="md-card md-card-hover">
                        <div class="gallery_grid_item md-card-content" align="center">
                            <a href="{{url('assets')}}/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
                                <img src="{{url('assets')}}/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
                            </a>
                            <div class="gallery_grid_image_caption">
                                <span class="gallery_image_title uk-text-truncate">&nbsp;</span>
                                <span class="uk-text-muted uk-text-small"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
			-->
			<!--
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <i class="md-icon material-icons md-card-toggle">&#xE316;</i> 
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                        <h3 class="md-card-toolbar-heading-text">
                            PREFIX OPERATOR
                        </h3>
                    </div>
                    <div class="md-card-content">
						<div id="prefix" style="height: 200px; widht: 100%;"></div>
					</div>
                </div>
            </div>
			-->
			<!-- 
            <div class="uk-width-medium-1-2">
                <div class="md-card">
                    <div class="md-card-toolbar">
                        <div class="md-card-toolbar-actions">
                            <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            <!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i>
                            <i class="md-icon material-icons md-card-close">&#xE14C;</i>
                        </div>
                            <h3 class="md-card-toolbar-heading-text">
                                
                            </h3>
                    </div>
                
                    <div class="md-card md-card-hover">
                        <div class="gallery_grid_item md-card-content" align="center">
                            <a href="{{url('assets')}}/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
                                <img src="{{url('assets')}}/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
                            </a>
                            <div class="gallery_grid_image_caption">
                                <span class="gallery_image_title uk-text-truncate">&nbsp;</span>
                                <span class="uk-text-muted uk-text-small"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
			-->
			<!-- BALANCE -->
			
        </div>
        </div>
    </div>

<script>
$(function(){
    document.getElementById("view1").innerHTML = document.getElementById("grafik").innerHTML ;
    document.getElementById("title1").innerHTML = "APPS" ;
	
    document.getElementById("view2").innerHTML = document.getElementById("grafik2").innerHTML ;
    document.getElementById("title2").innerHTML = "CONTENT PROVIDER - DELIVER" ;
});

function diva() {
    document.getElementById("view1").innerHTML = document.getElementById("grafik").innerHTML ;
    document.getElementById("title1").innerHTML = "APPS" ;
}
function divb() {
    document.getElementById("view1").innerHTML = document.getElementById("grafik2").innerHTML;
    document.getElementById("title1").innerHTML = "CONTENT PROVIDER - DELIVER" ;
}
</script>
@endsection
