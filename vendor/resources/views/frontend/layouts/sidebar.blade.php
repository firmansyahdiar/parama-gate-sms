<aside id="sidebar_main">

    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="/" class="sSidebar_hide"><img src="{{url('assets')}}/img/logo-gg.png" alt="" height="100" width="220"/></a>
            <a href="/" class="sSidebar_show"><img src="{{url('assets')}}/img/icon.png" alt="" height="32" width="32"/></a>
        </div>
    </div>

    <div class="menu_section">
        <ul>
            <!-- <li title="Dashboard">
                <a href="{{url('frontend/dashboard/')}}">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>
            </li> -->
            
			@foreach(Session::get('data_menus') as $k=>$menu)
				@if(empty($k))
					@foreach( $menu['child'] as $child )
					<li title="{{$child->name}}" class="<?php if (strtoupper($aktif) == strtoupper($child->name)) echo 'current_section' ?>">
						<a href="{{route($child->route)}}">
							<span class="menu_icon"><i class="material-icons">{{$child->icon}}</i></span>
							<span class="menu_title">{{$child->name}}</span>
						</a>
					</li>
					@endforeach
				@else
					<li title="{{ $menu['name'] }}" class="submenu_trigger">
						<a href="#">
							<span class="menu_icon"><i class="material-icons">{{ $menu['icon'] }}</i></span>
							<span class="menu_title">{{ $menu['name'] }}</span>
						</a>
						<ul>
							@foreach( $menu['child'] as $child )
							<li class="<?php if (strtoupper($aktif) == strtoupper($child->name)) echo 'act_item' ?>"><a title="{{$child->name}}" href="{{route($child->route)}}">{{$child->name}}</a></li>
							@endforeach
						</ul>
					</li>
				@endif
				
            @endforeach
			
        </ul>
    </div>
            
</aside>
