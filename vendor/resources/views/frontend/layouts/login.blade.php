<!doctype html>
<!--[if lte IE 9]> <html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Remove Tap Highlight on Windows Phone IE -->
    <meta name="msapplication-tap-highlight" content="no"/>

    <link rel="icon" type="image/png" href="{{url('assets')}}/img/icon.png" sizes="16x16">
    <link rel="icon" type="image/png" href="{{url('assets')}}/img/icon.png" sizes="32x32">

    <title>Login Page - SMS Masking</title>

    <link href='http://fonts.googleapis.com/css?family=Roboto:300,400,500' rel='stylesheet' type='text/css'>

    <!-- uikit -->
    <link rel="stylesheet" href="{{url('bower_components')}}/uikit/css/uikit.almost-flat.min.css"/>

    <!-- altair admin login page -->
    <link rel="stylesheet" href="{{url('assets')}}/css/login_page.min.css" />

</head>
<body class="login_page">

    <div class="login_page_wrapper">
        <div class="md-card" id="login_card">
            <div class="md-card-content large-padding" id="login_form">
                <div class="login_heading">
                    <img class="user_avatar" src="{{url('assets')}}/img/avatars/user2.png" alt=""/>
                </div>
                <!--<form>-->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
                    {{ csrf_field() }}
                    <div class="uk-form-row">
                        <label for="login_username">Email</label>
                        <input class="md-input" type="text" id="email" name="email" />
                    </div>
                    <div class="uk-form-row">
                        <label for="login_password">Password</label>
                        <input class="md-input" type="password" id="login_password" name="password" />
                    </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <!--<a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>-->
                                <!--<div class="uk-margin-top uk-text-center"><a href="#" id="forgot_form_show">Forgot Your Password</a></div>-->
                            </div>
                        </div>
<!--                    <div class="uk-margin-medium-top">
                        <a href="index.html" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign In</a>
                    </div>-->
                    <div class="uk-margin-top">
                        <a href="#" id="login_help_show" class="uk-float-right">Need help?</a>
                        <span class="icheck-inline">
                            <input type="checkbox" name="login_page_stay_signed" id="login_page_stay_signed" data-md-icheck />
                            <label for="login_page_stay_signed" class="inline-label">Stay signed in</label>
                        </span>
                    </div>
                </form>
            </div>
            <div class="md-card-content large-padding uk-position-relative" id="login_help" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_b uk-text-success">Can't log in?</h2>
                <p>Here�s the info to get you back in to your account as quickly as possible.</p>
                <p>First, try the easiest thing: if you remember your password but it isn�t working, make sure that Caps Lock is turned off, and that your username is spelled correctly, and then try again.</p>
                <p>If your password still isn�t working, it�s time to <a href="#" id="password_reset_show">reset your password</a>.</p>
            </div>
            <div class="md-card-content large-padding" id="login_password_reset" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-large-bottom">Reset password</h2>
                <!--<form>-->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                    {{ csrf_field() }}
                    <div class="uk-form-row">
                        <label for="login_email_reset">Your email address</label>
                        <input class="md-input" type="text" id="email" name="email" />
                    </div>
                    <div class="uk-margin-medium-top">
                                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                    <i class="fa fa-btn fa-sign-in"></i> Reset password
                                </button>
                        <!--<a href="index.html" class="md-btn md-btn-primary md-btn-block">Reset password</a>-->
                    </div>
                </form>
            </div>
            <!--CREATE ACCOUNT-->
            <div class="md-card-content large-padding" id="register_form" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-medium-bottom">Create an account</h2>
                <!--<form>-->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    <div class="uk-form-row">
                        <label for="register_username">Name</label>
                        <input class="md-input" type="text" id="name" name="name" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_email">E-mail</label>
                        <input class="md-input" type="text" id="email" name="email" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_password">Password</label>
                        <input class="md-input" type="password" id="password" name="password" />
                    </div>
                    <div class="uk-form-row">
                        <label for="register_password_repeat">Repeat Password</label>
                        <input class="md-input" type="password" id="password-confirm" name="password_confirmation" />
                    </div>
                    <div class="uk-margin-medium-top">
                                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                        <!--<a href="index.html" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign Up</a>-->
                    </div>
                </form>
            </div>
            <!--FORGOT PASSWORD-->
            <div class="md-card-content large-padding" id="forgot_form" style="display: none">
                <button type="button" class="uk-position-top-right uk-close uk-margin-right uk-margin-top back_to_login"></button>
                <h2 class="heading_a uk-margin-medium-bottom">Forgot Password</h2>
                <!--<form>-->
                <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                    {{ csrf_field() }}
                    <div class="uk-form-row">
                        <label for="register_email">E-mail</label>
                        <input class="md-input" type="text" id="email" name="email" />
                    </div>
                    <div class="uk-margin-medium-top">
                                <button type="submit" class="md-btn md-btn-primary md-btn-block md-btn-large">
                                    <i class="fa fa-btn fa-user"></i> Send Password Reset Link
                                </button>
                        <!--<a href="index.html" class="md-btn md-btn-primary md-btn-block md-btn-large">Sign Up</a>-->
                    </div>
                </form>
            </div>
        </div>
        <div class="uk-margin-top uk-text-center"><a href="#" id="signup_form_show">Create an account</a></div>
    </div>

    <!-- common functions -->
    <script src="{{url('assets')}}/js/common.min.js"></script>
    <!-- altair core functions -->
    <script src="{{url('assets')}}/js/altair_admin_common.min.js"></script>

    <!-- altair login page functions -->
    <script src="{{url('assets')}}/js/pages/login.min.js"></script>

</body>
</html>
