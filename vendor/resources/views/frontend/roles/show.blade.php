@extends($location.'.layouts.layouts')

@section('content')
		
    <div id="page_content">
        <div id="page_content_inner">

            <h4 class="heading_a uk-margin-bottom">{{$page_title}}</h4>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
                        @include($location.'.'.$view.'.show_fields')
						<!-- <div class="uk-text-right">
							<a href="{!! route('admin.'.$view.'.index') !!}" class="md-btn md-btn-flat md-btn-flat-primary">Back</a>
						</div> -->
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--BUTTON TAMBAH-->
    <div class="md-fab-wrapper">
        <!--<a class="md-fab md-fab-accent" data-uk-modal href="#modal_form" id="recordAdd">
        <a class="md-fab md-fab-accent" href="{!! route($location.'.'.$view.'.create') !!}" id="recordAdd">
            <i class="material-icons">&#xE145;</i>
        </a>-->
    </div>
	
@endsection
