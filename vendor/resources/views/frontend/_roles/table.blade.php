<table class="table table-responsive" id="categoryPosts-table">
    <thead>
        <th>Name</th>
        <th>Created</th>
        <th>Updated</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($datas as $data)
        <tr>
            <td class="uk-text-center">{!! $data->name !!}</td>
            <td class="uk-text-center">{!! $data->created_at !!}</td>
            <td class="uk-text-center">{!! $data->updated_at !!}</td>
            <td>
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route($location.'.'.$view.'.show', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
