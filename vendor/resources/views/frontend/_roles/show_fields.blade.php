{!! Form::open(['route' => [$location.'.'.$view.'.role_menus',$data->id]]) !!}

  <!-- Position Field -->
  <div class="form-group">
      {!! Form::label('menu', 'Menu:') !!}
      @foreach($listMenus as $key=>$list)
        <br/>
        {{ Form::checkbox('menu_id[]', $key, null) }}{{$list}}
      @endforeach
  </div>
  <div class="form-group col-sm-12">
      {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
      <a href="{!! route('admin.'.$view.'.index') !!}" class="btn btn-default">Cancel</a>
  </div>
{!! Form::close() !!}

  <div class="form-group">
      {!! Form::label('id', 'Id:') !!}
      <p>{!! $data->id !!}</p>
  </div>

  <!-- Parent Id Field -->
  <div class="form-group">
      {!! Form::label('name', 'Name:') !!}
      <p>{!! $data->name !!}</p>
  </div>

  <!-- Created At Field -->
  <div class="form-group">
      {!! Form::label('created_at', 'Created At:') !!}
      <p>{!! $data->created_at !!}</p>
  </div>

  <!-- Updated At Field -->
  <div class="form-group">
      {!! Form::label('updated_at', 'Updated At:') !!}
      <p>{!! $data->updated_at !!}</p>
  </div>
  <!-- Submit Field -->
