
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
				<h3 class="heading_a">Filter</h3><hr>
            </div>
{!! Form::open(['route' => $location.'.'.$view.'.store']) !!}
			<div class="uk-grid" data-uk-grid-margin>
				
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						{!! Form::label('masking', 'Masking : ') !!}
						{!! Form::select('masking', 
										$listMasking, 
										@$masking, 
										['class' => 'data-md-selectize']) !!}
					</div>
				</div>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						{!! Form::label('status', 'Status:') !!}
						{!! Form::select('status', array('-' => 'ALL', '1' => 'Pending', '2' => 'Deliver', '3' => 'Failed'), @$status, ['class' => 'data-md-selectize']) !!}
					</div>
				</div>
			</div>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								{!! Form::label('tgl_awal', 'Date Start :') !!}
								{!! Form::text('tgl_awal', $tgl_awal, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']) !!}
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								{!! Form::label('tgl_akhir', 'Date End :') !!}
								{!! Form::text('tgl_akhir', $tgl_akhir, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']) !!}
						</div>
					</div>
					<br>
				</div>
			</div>
        </div>
    </div>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-right">
					<button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> PROCESS </button>
				</div>
            </div>
        </div>
{!! Form::close() !!}
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-left">
						{!! Form::open(['url' => 'admin/report_maskings/clear_session']) !!}
							{!! Form::submit('RESET', ['class' => 'md-btn md-btn-flat md-btn-flat-primary']) !!}
						{!! Form::close() !!}
				</div>
            </div>
        </div>
    </div>
