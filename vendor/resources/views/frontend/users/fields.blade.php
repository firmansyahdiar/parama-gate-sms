
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
                  {!! Form::label('name', 'Name :') !!}
          <div class="uk-form-row">
                  {!! Form::text('name', null, ['class' => 'md-input']) !!}
          </div>
                  {!! Form::label('username', 'Username :') !!}
		  <div class="uk-form-row">
                  {!! Form::text('username', null, ['class' => 'md-input']) !!}
          </div>
          <!-- <div class="uk-form-row">
                  {!! Form::label('email', 'Email :') !!}
          </div> -->
                  {!! Form::hidden('email', null, ['class' => 'md-input']) !!}
                  {!! Form::label('password', 'Password :') !!}
          <div class="uk-form-row">
                  {!! Form::password('password', ['class' => 'md-input']) !!}
          </div>
                {!! Form::label('role_id', 'Role:') !!}
          <div class="uk-form-row">
                {!! Form::select('role_id', $listRoles, @$data->role_id, ['class' => 'data-md-selectize']) !!}
          </div>
          <!--<div class="uk-form-row">
                {!! Form::label('department_id', 'Department:') !!}
                {!! Form::select('department_id', $listDepartments, @$data->department_id, ['class' => 'data-md-selectize']) !!}
          </div>-->
		  <div class="uk-form-row">
                {!! Form::label('masking_id', 'Masking:') !!}
                @foreach( $listMaskings as $k=>$data )
				@if(!empty($user_masking[$k]))
				{!! Form::checkbox('masking_id[]', $k,true) !!}{!! $data !!}
				@else
				{!! Form::checkbox('masking_id[]', $k) !!}{!! $data !!}	
				@endif
				@endforeach
          </div>
		  
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="{!! route($location.'.'.$view.'.index') !!}"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
