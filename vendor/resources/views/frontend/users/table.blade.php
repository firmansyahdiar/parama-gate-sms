
<table class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">Name</th>
        <th class="uk-width-2-10 uk-text-center">User Name</th>
        <th class="uk-width-2-10 uk-text-center">Create</th>
        <th class="uk-width-2-10 uk-text-center">Update</th>
        <th colspan="2" class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
            <td class="uk-text-center">{{ $data->name }}</td>
            <td class="uk-text-center">{{ $data->username }}</td>
            <td class="uk-text-center">{{ $data->created_at }}</td>
            <td class="uk-text-center">{{ $data->updated_at }}</td>
            <td class="uk-text-right">
                <a title="Edit" href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                <!-- <a href="{!! route($location.'.'.$view.'.show', [$data->id]) !!}"><i class="md-icon material-icons">&#xE88F;</i></a> -->
            </td>
            <td class="">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

{{ $datas->links() }}