<!-- Name Field -->
    {!! Form::label('name', 'Name:') !!}
<div class="form-group col-sm-6">
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
    {!! Form::label('route', 'Route:') !!}
<div class="form-group col-sm-6">
    {!! Form::text('route', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route($location.'.'.$view.'.index') !!}" class="btn btn-default">Cancel</a>
</div>
