<?php $__env->startSection('content'); ?>

<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>
<script src="<?php echo e(url('assets')); ?>/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>
<br><br>
    <div id="page_content">
        <div id="page_content_inner">
          <!-- tasks -->
			<div class="uk-grid uk-grid-width-medium-1-3 uk-sortable sortable-handler" data-uk-grid="{gutter:24}" data-uk-sortable>
				<!-- <div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-content">
							<h3 class="heading_a" style = "text-align:center">APPS </h3>
							<div id="grafik" style="height: 400px; widht: 100%;"></div>
						</div>
					</div>
				</div> -->
				<!-- <div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-content" style = "text-align:center">
							<h3 class="heading_a ">CONTENT PROVIDER </h3>
							<div id="grafik2" style="height: 400px; widht: 100%;"></div>
						</div>
					</div>
				</div> -->
				<div class="uk-width-medium-1-2">
							<div class="md-card">
								<div class="md-card-toolbar">
									<div class="md-card-toolbar-actions">
										<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
										<i class="md-icon material-icons md-card-close">&#xE14C;</i>
									</div>
									<h3 class="md-card-toolbar-heading-text">
										APPS 
									</h3>
								</div>
								<div class="md-card-content">
									<div id="grafik" style="height: 200px; widht: 100%;"></div>
									<div class="uk-grid" data-uk-grid-margin>
										<div class="uk-width-medium-2-3">
											<div class="uk-form-row">
												Last Reset Counter : <?php echo e($select_masking->graph_app); ?>

											</div>
										</div>
										<div class="uk-width-medium-1-3">
											<div class="uk-form-row" align="right">
												<a href="<?php echo e(url('/frontend/dashboard/reset_app/'.$user_masking[0])); ?>" class="user_action_image" title="Reset">
													<i class="material-icons">&#xE3AE;</i>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									CONTENT PROVIDER
								</h3>
						</div>
						<div class="md-card-content">
							<div id="grafik2" style="height: 200px; widht: 100%;"></div>
							<div class="uk-grid" data-uk-grid-margin>
								<div class="uk-width-medium-2-3">
									<div class="uk-form-row">
										Last Reset Counter : <?php echo e($select_masking->graph_delivery); ?>

									</div>
								</div>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row" align="right">
										<a href="<?php echo e(url('/frontend/dashboard/reset_delivery/'.$user_masking[0])); ?>" class="user_action_image" title="Reset">
											<i class="material-icons">&#xE3AE;</i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									CONTENT PROVIDER - DELIVER
								</h3>
						</div>
						<div class="md-card-content">
							<div id="grafik_cp_deliver" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									CONTENT PROVIDER - FAILED
								</h3>
						</div>
						<div class="md-card-content">
							<div id="grafik_cp_failed" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-1">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									BALANCE
								</h3>
						</div>
						<div class="md-card-content">
							<div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-4 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
							<?php foreach($masking as $data): ?>
							<?php
							$url = url('/api/sendings/get_balance', $data->masking);
							$json = @file_get_contents($url);
							$balance = json_decode($json, true);
							?>
							<div>
								<div class="md-card">
									<div class="md-card-content" style="Background: #DCDCDC;">
										<div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
										<span class="uk-text-muted uk-text-small"><font color="#000000;"> <?php echo e(strtoupper($data->masking)); ?> </font></span>
										<h2 class="uk-margin-remove">&nbsp
											<?php 
												if (($data->quota*$data->critical_notif)/100 >= $balance) {
													$color="color:red;";
													$kedip="berkedip";
												}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
													$color="color:blue;";
													$kedip="berkedip";
												}else{
													$color="";
													$kedip="";
												}
											?> 
											<span class="countUpMe <?php echo e($kedip); ?>" style="<?php echo e($color); ?>">
												 <?php echo e($balance); ?> 
											</span>
										</h2>
									</div>
								</div>
							</div>
							<?php endforeach; ?>
						</div>
						</div>
					</div>
				</div>
				<!-- 
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-toggle">&#xE316;</i> 
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									
								</h3>
						</div>
					
						<div class="md-card md-card-hover">
							<div class="gallery_grid_item md-card-content" align="center">
								<a href="<?php echo e(url('assets')); ?>/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
									<img src="<?php echo e(url('assets')); ?>/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
								</a>
								<div class="gallery_grid_image_caption">
									<span class="gallery_image_title uk-text-truncate">&nbsp</span>
									<span class="uk-text-muted uk-text-small"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								 <i class="md-icon material-icons md-card-toggle">&#xE316;</i> 
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 class="md-card-toolbar-heading-text">
								PREFIX OPERATOR
							</h3>
						</div>
						<div class="md-card-content">
							<div id="prefix" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									
								</h3>
						</div>
					
						<div class="md-card md-card-hover">
							<div class="gallery_grid_item md-card-content" align="center">
								<a href="<?php echo e(url('assets')); ?>/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
									<img src="<?php echo e(url('assets')); ?>/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
								</a>
								<div class="gallery_grid_image_caption">
									<span class="gallery_image_title uk-text-truncate">&nbsp</span>
									<span class="uk-text-muted uk-text-small"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				-->
			</div>

			<!--BUTTON TAMBAH
			<div class="md-fab-wrapper">
                <div class="md-fab md-fab-accent md-fab-sheet">
                    <i class="material-icons">&#xE254;</i>
                    <div class="md-fab-sheet-actions">
                        <a href="<?php echo route($location.'.'.$view.'.create'); ?>" class="md-color-white"><i class="material-icons md-color-white">&#xE872;</i> Clear History Graphic</a>
                    </div>
                </div>
			</div>
			-->
        </div>
    </div>
	
<?php $__env->stopSection(); ?>

<?php echo $__env->make($location.'.layouts.dashboard', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>