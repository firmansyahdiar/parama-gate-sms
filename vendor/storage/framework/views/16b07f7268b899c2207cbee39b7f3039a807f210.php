
    <div class="uk-grid" data-uk-grid-margin>
          <div class="uk-width-medium-1-2">
                  <?php echo Form::label('name', 'Name :'); ?>

          <div class="uk-form-row">
                  <?php echo Form::text('name', null, ['class' => 'md-input']); ?>

          </div>
                  
          <!-- <div class="uk-form-row">
                  <?php echo Form::label('email', 'Email :'); ?>

          </div> -->
                  <?php echo Form::hidden('email', null, ['class' => 'md-input']); ?>

                  <?php echo Form::label('password', 'Password :'); ?>

          <div class="uk-form-row">
                  <?php echo Form::password('password', ['class' => 'md-input']); ?>

          </div>
                
          <!--<div class="uk-form-row">
                <?php echo Form::label('department_id', 'Department:'); ?>

                <?php echo Form::select('department_id', $listDepartments, @$data->department_id, ['class' => 'data-md-selectize']); ?>

          </div>-->
		  
		  
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="<?php echo route($location.'.'.$view.'.index'); ?>"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
