


<table>
	<!-- <tr>
		<td width="100"> <?php echo Form::label('id', 'ID'); ?> </td>
		<td> : </td>
		<td> <?php echo $data->id; ?> </td>
	</tr> -->
	<tr>
		<td> <?php echo Form::label('name', 'Name'); ?> </td>
		<td> : </td>
		<td> <?php echo $data->name; ?>  </td>
	</tr>
	<tr>
		<td> <?php echo Form::label('created_at', 'Created At '); ?> </td>
		<td> : </td>
		<td> <?php echo $data->created_at; ?>  </td>
	</tr>
	<tr>
		<td> <?php echo Form::label('updated_at', 'Updated At '); ?> </td>
		<td> : </td>
		<td> <?php echo $data->updated_at; ?>  </td>
	</tr>
</table>

<hr><h3 class="heading_a">Menu</h3><hr>
  
<?php echo Form::open(['url' => 'frontend/roles/role_menus/'.$data->id]); ?>

  <!-- Position Field -->
  
<style>
div.ex1 {
    margin-left: 40px;
}
</style>
	<table>
		<?php foreach($listMenus as $key): ?>
		<tr>
			<td width="300" class="ex1"> 
				<?php if($key->parent_id == "0"): ?>
					<b><?php echo $key->name; ?> </b>
				<?php else: ?>
					<div class="ex1"> <?php echo $key->name; ?>  </div>
				<?php endif; ?>
			</td>
			<td> 
				<?php if($key->route != " "): ?>
					<?php if(!empty($menus[$key->id])): ?>
						<!-- <?php echo Form::checkbox('menu_id[]', $key,true); ?> -->
						<?php echo Form::checkbox('menu_id[]', $key->id, ['class' => '']); ?>

					<?php else: ?>
						<?php echo Form::checkbox('menu_id[]', $key->id); ?>

					<?php endif; ?>
				<?php else: ?>
					<?php echo ''; ?>

				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
  <br>
  <div class="uk-text-right">
      <a href="<?php echo route('frontend.'.$view.'.index'); ?>" class="btn btn-default">CANCEL</a>
      <?php echo Form::submit('Save', ['class' => 'md-btn md-btn-flat md-btn-flat-primary']); ?>

  <?php echo Form::close(); ?>

  </div>