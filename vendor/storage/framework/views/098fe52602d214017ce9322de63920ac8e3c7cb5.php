<aside id="sidebar_main">

    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="/admin" class="sSidebar_hide"><img src="<?php echo e(url('assets')); ?>/img/logo-gg.png" alt="" height="100" width="220"/></a>
            <a href="/admin" class="sSidebar_show"><img src="<?php echo e(url('assets')); ?>/img/icon.png" alt="" height="32" width="32"/></a>
        </div>
    </div>

    <div class="menu_section">
        <ul>
            <li title="Home" class="<?php if ($view == 'dashboard') echo 'current_section' ?>">
                <a href="<?php echo e(url('admin/dashboard')); ?>">
                    <span class="menu_icon"><i class="material-icons">&#xE88A;</i></span>
                    <span class="menu_title">HOME</span>
                </a>
            </li>
            <li title="Upload" class="<?php if ($view == 'file_histories') echo 'current_section' ?>">
                <a href="<?php echo e(route('admin.file_histories.index')); ?>">
                    <span class="menu_icon"><i class="material-icons">&#xE864;</i></span>
                    <span class="menu_title">UPLOAD</span>
                </a>
            </li>
            <li title="User Management">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE7FD;</i></span>
                    <span class="menu_title">USER MANAGEMENT</span>
                </a>
                <ul>
                    <li title="Masking ID" class="<?php if ($view == 'maskings') echo 'act_item' ?>"><a title="ID Masking" href="<?php echo e(route('admin.maskings.index')); ?>">Masking ID</a></li>
                    <li title="Users" class="<?php if ($view == 'users') echo 'act_item' ?>"><a href="<?php echo e(route('admin.users.index')); ?>">User</a></li>
                    <li title="Admins" class="<?php if ($view == 'admins') echo 'act_item' ?>"><a href="<?php echo e(route('admin.admins.index')); ?>">Admin</a></li>
                    <li title="Roles" class="<?php if ($view == 'roles') echo 'act_item' ?>"><a href="<?php echo e(route('admin.roles.index')); ?>">Roles</a></li>
                    <li title="Roles" class="<?php if ($view == 'alerts') echo 'act_item' ?>"><a href="<?php echo e(route('admin.alerts.index')); ?>">Alerts</a></li>
                </ul>
            </li>
            <li title="Report">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE8AD;</i></span>
                    <span class="menu_title">REPORT</span>
                </a>
                <ul>
                    <li title="Delivery" class="<?php if ($view == 'report_maskings') echo 'act_item' ?>"><a href="<?php echo e(route('admin.report_maskings.index')); ?>">Delivery</a></li> 
                    <li title="Delivery" class="<?php if ($view == 'unsents') echo 'act_item' ?>"><a title="Unsent" href="<?php echo e(route('admin.unsents.index')); ?>">Unsent</a></li>
                </ul>
            </li>
<!--                <li title="Snippets">
                <a href="page_snippets.html">
                    <span class="menu_icon"><i class="material-icons">&#xE86F;</i></span>
                    <span class="menu_title">Laporan</span>
                </a>
                <ul>
                    <li class="menu_subtitle">Penjualan</li>
                    <li><a href="#">Per-Supplier</a></li>
                    <li><a href="#">Per-Periode</a></li>
                    <li class="menu_subtitle">Pembelian</li>
                    <li><a href="#">Per-Customer</a></li>
                    <li><a href="#">Per-Periode</a></li>
                </ul>
            </li>-->
<!--                <li title="User Profile">
                <a href="/profile">
                    <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
                    <span class="menu_title">About</span>
                </a>
            </li>-->
        </ul>
    </div>
</aside>
