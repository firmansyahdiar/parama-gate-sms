
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
                    <?php echo Form::label('name', 'Name :'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('name', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('route', 'Route:'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('route', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('icon', 'Icon:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('icon', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('sort', 'Sort:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('sort', null, ['class' => 'md-input']); ?>

            </div>
			<div class="uk-form-row">
                <?php echo Form::label('parent_id', 'Parent Menu:'); ?>

                <?php echo Form::select('parent_id', $listMenus, @$data->parent_id, ['class' => 'data-md-selectize']); ?>

          </div>
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="<?php echo route('admin.'.$view.'.index'); ?>"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
