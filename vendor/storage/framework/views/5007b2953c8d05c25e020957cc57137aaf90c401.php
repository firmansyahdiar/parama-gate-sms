
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
                  <?php echo Form::label('name', 'Name :'); ?>

          <div class="uk-form-row">
                  <?php echo Form::text('name', null, ['class' => 'md-input']); ?>

          </div>
                  <?php echo Form::label('username', 'Username :'); ?>

		  <div class="uk-form-row">
                  <?php echo Form::text('username', null, ['class' => 'md-input']); ?>

          </div>
          <!-- <div class="uk-form-row">
                  <?php echo Form::label('email', 'Email :'); ?>

          </div> -->
                  <?php echo Form::hidden('email', null, ['class' => 'md-input']); ?>

                  <?php echo Form::label('password', 'Password :'); ?>

          <div class="uk-form-row">
                  <?php echo Form::password('password', ['class' => 'md-input']); ?>

          </div>
                <?php echo Form::label('role_id', 'Role:'); ?>

          <div class="uk-form-row">
                <?php echo Form::select('role_id', $listRoles, @$data->role_id, ['class' => 'data-md-selectize']); ?>

          </div>
          <!--<div class="uk-form-row">
                <?php echo Form::label('department_id', 'Department:'); ?>

                <?php echo Form::select('department_id', $listDepartments, @$data->department_id, ['class' => 'data-md-selectize']); ?>

          </div>-->
		  <div class="uk-form-row">
                <?php echo Form::label('masking_id', 'Masking:'); ?>

                <?php foreach( $listMaskings as $k=>$data ): ?>
				<?php if(!empty($user_masking[$k])): ?>
				<?php echo Form::checkbox('masking_id[]', $k,true); ?><?php echo $data; ?>

				<?php else: ?>
				<?php echo Form::checkbox('masking_id[]', $k); ?><?php echo $data; ?>	
				<?php endif; ?>
				<?php endforeach; ?>
          </div>
		  
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="<?php echo route($location.'.'.$view.'.index'); ?>"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
