<?php $__env->startSection('content'); ?>
	
	    <div id="page_content">
        <div id="page_content_inner">

            <h4 class="heading_a uk-margin-bottom"><?php echo e($page_title); ?></h4>
            <div class="md-card uk-margin-medium-bottom">
				<div class="md-card-content">
					<?php echo Form::open(['route' => $location.'.'.$view.'.store', 'files' => true]); ?>


						<!-- Picture Field -->
						<div class="form-group col-sm-12">
							<?php echo Form::label('file', 'Upload File :  ', ['class' => 'heading_a']); ?>

							<?php echo Form::file('file',['class' => 'uk-form-file md-btn','id'=>'inputFile']); ?>

							
							<?php echo Form::submit('Save', ['class' => 'uk-form-file md-btn']); ?>

							<a href="<?php echo route('frontend.'.$view.'.index'); ?>" class="uk-form-file md-btn">Cancel</a>
						</div>
					<?php echo Form::close(); ?>

                </div>
			</div>
				
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-overflow-container">
					<?php echo $__env->make($location.'.'.$view.'.table', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <!--BUTTON TAMBAH-->
    <!--<div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" data-uk-modal href="#modal_form" id="recordAdd">
        <a class="md-fab md-fab-accent" href="<?php echo route($location.'.'.$view.'.create'); ?>" id="recordAdd">
            <i class="material-icons">&#xE145;</i>
        </a>
    </div>-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make($location.'.layouts.layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>