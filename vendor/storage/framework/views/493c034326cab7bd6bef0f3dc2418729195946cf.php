
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}												
</style>
<script src="<?php echo e(url('assets')); ?>/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>

<table class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">User Name</th>
        <th class="uk-width-1-10 uk-text-center">Password</th>
        <th class="uk-width-2-10 uk-text-center">Masking</th>
		<th class="uk-width-1-10 uk-text-center">Quota</th>
        <th class="uk-width-1-10 uk-text-center">Medium</th>
        <th class="uk-width-1-10 uk-text-center">Critical</th>
        <th class="uk-width-1-10 uk-text-center">Action</th>
        <th class="uk-width-2-10 uk-text-center">Balance</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($datas as $data): ?>
        <tr>
		<?php
		$url = url('api/sendings/get_balance', $data->masking);
		$json = @file_get_contents($url);
		$balance = json_decode($json, true);
		?>
            <td class="uk-text-center"><?php echo e($data->username); ?></td>
            <td class="uk-text-center"><?php echo e($data->password); ?></td>
            <td class="uk-text-center"><?php echo e($data->masking); ?></td>
			<td class="uk-text-center"><?php echo e($data->quota); ?></td>
            <td class="uk-text-center"><?php echo e($data->medium_notif); ?></td>
            <td class="uk-text-center"><?php echo e($data->critical_notif); ?></td>
            <td class="uk-width-2-10 uk-text-center">
                <?php echo Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']); ?>

                <a title="Edit" href="<?php echo route($location.'.'.$view.'.edit', [$data->id]); ?>"><i class="md-icon material-icons">&#xE254;</i></a>
                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                <?php echo Form::close(); ?>

            </td>
										<?php 
											if (($data->quota*$data->critical_notif)/100 >= $balance) {
												$color="color:red;";
												$kedip="berkedip";
											}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
												$color="color:blue;";
												$kedip="berkedip";
											}else{
												$color="";
												$kedip="";
											}
										?> 
            <td class="uk-text-right">
										<span class="<?php echo e($kedip); ?>" style="<?php echo e($color); ?>">
											 <?php echo e(ceil($balance)); ?> 
										</span>
			</td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php echo e($datas->links()); ?>