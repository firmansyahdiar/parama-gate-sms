
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">Phone Number</th>
        <th class="uk-width-2-10 uk-text-center">Message</th>
        <th class="uk-width-2-10 uk-text-center">Created</th>
        <th class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($datas as $data): ?>
        <tr>
            <td class="uk-text-center"><?php echo $data->phone_number; ?></td>
            <td class="uk-text-center"><?php echo substr($data->message,0,20); ?></td>
            <td class="uk-text-center"><?php echo e($data->created_at); ?></td>
            <td class="uk-text-center">
                <?php echo Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']); ?>

                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php echo e($datas->links()); ?>

