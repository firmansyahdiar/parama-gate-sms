<aside id="sidebar_main">

    <div class="sidebar_main_header">
        <div class="sidebar_logo">
            <a href="/" class="sSidebar_hide"><img src="<?php echo e(url('assets')); ?>/img/logo-gg.png" alt="" height="100" width="220"/></a>
            <a href="/" class="sSidebar_show"><img src="<?php echo e(url('assets')); ?>/img/icon.png" alt="" height="32" width="32"/></a>
        </div>
    </div>

    <div class="menu_section">
        <ul>
            <!-- <li title="Dashboard">
                <a href="<?php echo e(url('frontend/dashboard/')); ?>">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>
            </li> -->
            
			<?php foreach(Session::get('data_menus') as $k=>$menu): ?>
				<?php if(empty($k)): ?>
					<?php foreach( $menu['child'] as $child ): ?>
					<li title="<?php echo e($child->name); ?>" class="<?php if (strtoupper($aktif) == strtoupper($child->name)) echo 'current_section' ?>">
						<a href="<?php echo e(route($child->route)); ?>">
							<span class="menu_icon"><i class="material-icons"><?php echo e($child->icon); ?></i></span>
							<span class="menu_title"><?php echo e($child->name); ?></span>
						</a>
					</li>
					<?php endforeach; ?>
				<?php else: ?>
					<li title="<?php echo e($menu['name']); ?>" class="submenu_trigger">
						<a href="#">
							<span class="menu_icon"><i class="material-icons"><?php echo e($menu['icon']); ?></i></span>
							<span class="menu_title"><?php echo e($menu['name']); ?></span>
						</a>
						<ul>
							<?php foreach( $menu['child'] as $child ): ?>
							<li class="<?php if (strtoupper($aktif) == strtoupper($child->name)) echo 'act_item' ?>"><a title="<?php echo e($child->name); ?>" href="<?php echo e(route($child->route)); ?>"><?php echo e($child->name); ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>
				<?php endif; ?>
				
            <?php endforeach; ?>
			
        </ul>
    </div>
            
</aside>
