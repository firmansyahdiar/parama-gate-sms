
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">Masking</th>
        <th class="uk-width-2-10 uk-text-center">Phone Number</th>
        <th colspan="2" class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($datas as $data): ?>
        <tr>
            <td class="uk-text-center"><?php echo e(@$data->masking->masking); ?></td>
            <td class="uk-text-center"><?php echo e($data->phone_number); ?></td>
            <td class="uk-text-right">
                <a title="Edit" href="<?php echo route($location.'.'.$view.'.edit', [$data->id]); ?>"><i class="md-icon material-icons">&#xE254;</i></a>
                <!-- <a href="<?php echo route($location.'.'.$view.'.show', [$data->id]); ?>"><i class="md-icon material-icons">&#xE88F;</i></a> -->
            </td>
            <td class="">
                <?php echo Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']); ?>

                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
