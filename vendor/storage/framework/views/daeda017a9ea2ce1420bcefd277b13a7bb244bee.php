<header id="header_main">
    <div class="header_main_content">
			<div align="right">
                <!-- <font face="verdana" color="green" size="3">GATE-SMS &nbsp;&nbsp;&nbsp;</font> -->
                <img src="<?php echo e(url('assets')); ?>/img/logo-system.png" alt="" height="60" width="150"/>
			</div>
        <nav class="uk-navbar">

            <!-- main sidebar switch -->
			<!-- <img src="<?php echo e(url('assets')); ?>/img/menu.png" alt="" height="200" width="200"/> -->
			<!--
            <a title="Show/Hide Menu" href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                 <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            </a>
			-->

            <!-- secondary sidebar switch -->
			<!-- <img src="<?php echo e(url('assets')); ?>/img/menu.png" alt="" height="200" width="200"/> -->
			<!--
            <a title="Show/Hide Menu" href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                 <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
            </a>
			-->
			<!--
			<div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
						<a href="#" class="top_menu_toggle">
							<input type="checkbox" name="style_sidebar_mini" id="style_sidebar_mini" data-md-icheck />
							<label for="style_sidebar_mini" class="inline-label">Small Menu</label>
						</a>
					</div>
			</div>
			-->
            <!-- BOX MENU ATAS

                <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                        <div class="uk-dropdown uk-dropdown-width-3">
                            <div class="uk-grid uk-dropdown-grid" data-uk-grid-margin>
                                <div class="uk-width-2-3">
                                    <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-top uk-margin-bottom uk-text-center" data-uk-grid-margin>
                                        <a href="page_mailbox.html">
                                            <i class="material-icons md-36">&#xE158;</i>
                                            <span class="uk-text-muted uk-display-block">Mailbox</span>
                                        </a>
                                        <a href="page_invoices.html">
                                            <i class="material-icons md-36">&#xE53E;</i>
                                            <span class="uk-text-muted uk-display-block">Invoices</span>
                                        </a>
                                        <a href="page_chat.html">
                                            <i class="material-icons md-36 md-color-red-600">&#xE0B9;</i>
                                            <span class="uk-text-muted uk-display-block">Chat</span>
                                        </a>
                                        <a href="page_scrum_board.html">
                                            <i class="material-icons md-36">&#xE85C;</i>
                                            <span class="uk-text-muted uk-display-block">Scrum Board</span>
                                        </a>
                                        <a href="page_snippets.html">
                                            <i class="material-icons md-36">&#xE86F;</i>
                                            <span class="uk-text-muted uk-display-block">Snippets</span>
                                        </a>
                                        <a href="page_user_profile.html">
                                            <i class="material-icons md-36">&#xE87C;</i>
                                            <span class="uk-text-muted uk-display-block">User profile</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="uk-width-1-3">
                                    <ul class="uk-nav uk-nav-dropdown uk-panel">
                                        <li class="uk-nav-header">Components</li>
                                        <li><a href="components_accordion.html">Accordions</a></li>
                                        <li><a href="components_buttons.html">Buttons</a></li>
                                        <li><a href="components_notifications.html">Notifications</a></li>
                                        <li><a href="components_sortable.html">Sortable</a></li>
                                        <li><a href="components_tabs.html">Tabs</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
           -->

            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <!-- <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li> -->
                    <!-- NOTIF
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
                        <div class="uk-dropdown uk-dropdown-xlarge">
                            <div class="md-card-content">
                                <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                                    <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                                    <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                                </ul>
                                <ul id="header_alerts" class="uk-switcher uk-margin">
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-cyan">jx</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="pages_mailbox.html">Iure et nam.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Vel aut officiis commodi veritatis dicta.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="<?php echo e(url('assets')); ?>/img/avatars/avatar_07_tn.png" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="pages_mailbox.html">Voluptate explicabo.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Doloribus qui expedita debitis saepe quidem vel impedit dolore dolore qui est.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <span class="md-user-letters md-bg-light-green">lm</span>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="pages_mailbox.html">Minima perspiciatis.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Quas cumque doloremque ab aut aut quia veritatis.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="<?php echo e(url('assets')); ?>/img/avatars/avatar_02_tn.png" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="pages_mailbox.html">Aperiam dolorum corporis.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Delectus dicta eaque illum ad nemo.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <img class="md-user-image md-list-addon-avatar" src="<?php echo e(url('assets')); ?>/img/avatars/avatar_09_tn.png" alt=""/>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading"><a href="pages_mailbox.html">Non harum unde.</a></span>
                                                    <span class="uk-text-small uk-text-muted">Sequi molestias non provident similique adipisci ullam.</span>
                                                </div>
                                            </li>
                                        </ul>
                                        <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                                            <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                                        </div>
                                    </li>
                                    <li>
                                        <ul class="md-list md-list-addon">
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Sit doloribus voluptas.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Harum velit assumenda dolorem ducimus ut officiis.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Molestias libero.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Magnam enim quia aut tenetur error numquam.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Et et.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Aliquam eius id qui illum.</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-addon-element">
                                                    <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                                                </div>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Non ad.</span>
                                                    <span class="uk-text-small uk-text-muted uk-text-truncate">Et inventore aperiam velit.</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </li> 
					-->
					<li>
						<br>
						<b>Welcome <?php echo auth()->user()->name; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
					</li>
					<li>
						<!-- main sidebar switch -->
						<a title="Show/Hide Menu" href="#" id="sidebar_main_toggle" class="user_action_image">
							 <i class="md-icon material-icons">&#xE5D0;</i>
						</a>
					</li>
					<li>
						<a href="<?php echo e($view); ?>" title="Refresh" class="user_action_image">
							<i class="md-icon material-icons">&#xE5D5;</i>
						</a>
					</li>
                    <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                        <a href="#" title="Tools" class="user_action_image"><i class="md-icon material-icons">&#xE8B8;</i></a>
                        <div class="uk-dropdown uk-dropdown-small">
                            <ul class="uk-nav js-uk-prevent">
                                <!--<li><a href="page_user_profile.html">My profile</a></li>
                                <li><a href="page_settings.html">Settings</a></li>-->
								<!--<li><a href="#" id="full_screen_toggle">Full Screen</a></li>-->
                                <li><a href="#" data-uk-modal="{target:'#modal_reset'}">Edit Profile</a></li>
                                <!-- <li><a href="#" data-uk-modal="{target:'#modal_overflow'}">About</a></li> -->
                                <li><a href="<?php echo e(url('/logout')); ?>">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
							<!-- ABOUT -->
							<div id="modal_overflow" class="uk-modal">
                                <div class="uk-modal-dialog">
                                    <button type="button" class="uk-modal-close uk-close"></button>
									<img src="<?php echo e(url('assets')); ?>/img/logo-system.png" alt="" />
                                    <p>Version 3.1 <br> GATE-SMS is a SMS Gateway aplication created by bla bla bla bla</p><hr>
									<div class="uk-overflow-container">
                                        <h2 class="heading_b">Whats new in 1.2 (2016-Jun-18): </h2>
                                        <p> *) Fixing problem GUI in Admin side. When create a new user found bla bla bla; <br>
											*) add tabel lenght sms in report delivery;</p>
										
										<hr>
										<h2 class="heading_b">Whats new in 1.1 (2016-Jun-12):</h2>
										<p> *) dfgfdghfdgdf.fdgdfgdfg.fdgfdgfdgdfgfd; <br>
										*) dsfdfdsfgdswerytijuy ytytui yityiuuyi utiuy  <br>
										*) gjyjyt ytjytjytj ytjytj</p>

										<hr>
										<h2 class="heading_b">Aplication version 1.0 (2016-Jun-01):</h2>
										<p> *)Feature a <br>
										*)Feature b <br>
										*)Feature c <br>
										*)Feature d <br>
										*)Feature e <br>
										*)Feature f <br>
										*)Feature g <br>
										*)Feature h <br>
										*)Feature i <br> 
										*)Using MSSQL DB </p>
                                    </div>
                                </div>
                            </div>
							<!-- RESET PASSWORD -->
							<div id="modal_reset" class="uk-modal">
                                <div class="uk-modal-dialog">
                                    <button type="button" class="uk-modal-close uk-close"></button>
									<?php echo Form::open(['route' => [$location.'.users.update_profile',Auth::user()->id], 'method' => 'patch']); ?>

										<h1> EDIT PROFILE </h1><hr>
										<div class="uk-grid" data-uk-grid-margin>
											<div class="uk-width-medium-1-2">
														  <?php echo Form::label('name', 'Name :'); ?>

												  <div class="uk-form-row">
														  <?php echo Form::text('name', Auth::user()->name, ['class' => 'md-input']); ?>

												  </div>
														<?php echo Form::label('password', 'New Password :'); ?>

												  <div class="uk-form-row">
														<?php echo Form::password('password', ['class' => 'md-input']); ?>

												  </div>
											</div>
										</div>
										<div class="uk-text-right">
											<a class="md-btn md-btn-flat uk-modal-close" href="#"> Cancel</a>
											<button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
										</div>
									<?php echo Form::close(); ?>

                                </div>
                            </div>
        </nav>
    </div>
    <div class="header_main_search_form">
        <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
        <form class="uk-form">
            <input type="text" class="header_main_search_input" />
            <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
        </form>
    </div>
</header>
