
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
                    <?php echo Form::label('username', 'Username:'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('username', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('password', 'Password:'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('password', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('masking', 'Masking:'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('masking', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('quota', 'Quota:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('quota', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('medium_notif', 'Medium Notif (%):'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('medium_notif', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('medium_max', 'Medium Max SMS:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('medium_max', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('medium_interval', 'Medium Interval:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('medium_interval', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('critical_notif', 'Critical Notif (%):'); ?>

            <div class="uk-form-row">
                    <?php echo Form::text('critical_notif', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('critical_max', 'Critical Max SMS:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('critical_max', null, ['class' => 'md-input']); ?>

            </div>
                    <?php echo Form::label('critical_interval', 'Critical Interval:'); ?>

			<div class="uk-form-row">
                    <?php echo Form::text('critical_interval', null, ['class' => 'md-input']); ?>

            </div>
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="<?php echo route('admin.'.$view.'.index'); ?>"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
