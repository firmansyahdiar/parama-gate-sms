
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
				<h3 class="heading_a">Filter</h3><hr>
            </div>
<?php echo Form::open(['route' => $location.'.'.$view.'.store']); ?>

			<div class="uk-grid" data-uk-grid-margin>
			
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						<?php echo Form::label('masking', 'Masking : '); ?>

						<!-- <?php echo Form::select('masking', 
										$listMasking, 
										@$masking, 
										['class' => 'data-md-selectize']); ?> -->
										
                        <select id="masking" name="masking" data-md-selectize>
                                <!-- <option value="-" >ALL</option> -->
							<?php foreach($listMasking as $listMasking): ?>
								<?php
								if ($listMasking->id == $masking){
									$pilih="selected";
								}else{
									$pilih="";
								}
								?>
                                <option value="<?php echo e($listMasking->masking_id); ?>" <?php echo e($pilih); ?>><?php echo e(@$listMasking->masking->masking); ?></option>
							<?php endforeach; ?>
                        </select>
                    </div>
				</div>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
						<?php echo Form::label('status', 'Status:'); ?>

						<?php echo Form::select('status', array('-' => 'ALL', '1' => 'Pending', '2' => 'Deliver', '3' => 'Failed'), @$status, ['class' => 'data-md-selectize']); ?>

					</div>
				</div>
			</div>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
								<?php echo Form::label('tgl_awal', 'Start Date :'); ?>

						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								<?php echo Form::text('tgl_awal', $tgl_awal, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']); ?>

						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
								<?php echo Form::label('tgl_akhir', 'End Date :'); ?>

						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								<?php echo Form::text('tgl_akhir', $tgl_akhir, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']); ?>

						</div>
					</div>
					<br>
				</div>
			</div>
        </div>
    </div>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-right">
					<button type="submit" class="md-btn md-btn-flat-primary" id="snippet_new_save"> PROCESS </button>
<?php echo Form::close(); ?>

				</div>
            </div>
        </div>
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-left">
						<?php echo Form::open(['url' => 'frontend/reportmaskings/clear_session']); ?>

							<?php echo Form::submit('RESET', ['class' => 'md-btn md-btn-flat-primary']); ?>

						<?php echo Form::close(); ?>

				</div>
            </div>
        </div>
    </div>


		<!-- GRAFIK 
        <div class="uk-width-medium-2-4">
            <div class="uk-form-row">
                                <table class="uk-table">
                                    <thead>
                                        <tr>
                                            <th class="uk-text-nowrap">Status</th>
                                            <th class="uk-text-nowrap">Progress</th>
                                            <th class="uk-text-nowrap uk-text-right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="uk-table-middle">
                                            <td class="uk-width-2-10"><span class="uk-badge uk-badge-success">Deliver</span></td>
                                            <td class="uk-width-3-10">
                                                <div class="uk-progress uk-progress-mini uk-progress-success uk-margin-remove">
                                                    <div class="uk-progress-bar" style="width: 60%;"></div>
                                                </div>
                                            </td>
                                            <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small"><?php echo $deliver; ?></td>
                                        </tr>
                                        <tr class="uk-table-middle">
                                            <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-warning">Pending</span></td>
                                            <td class="uk-width-3-10">
                                                <div class="uk-progress uk-progress-mini uk-progress-success uk-margin-remove">
                                                    <div class="uk-progress-bar" style="width: 15%;"></div>
                                                </div>
                                            </td>
                                            <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small"><?php echo $pending; ?></td>
                                        </tr>
                                        <tr class="uk-table-middle">
                                            <td class="uk-width-2-10 uk-text-nowrap"><span class="uk-badge uk-badge-danger">Failed</span></td>
                                            <td class="uk-width-3-10">
                                                <div class="uk-progress uk-progress-mini uk-progress-danger uk-margin-remove">
                                                    <div class="uk-progress-bar" style="width: 25%;"></div>
                                                </div>
                                            </td>
                                            <td class="uk-width-2-10 uk-text-right uk-text-muted uk-text-small"><?php echo $failed; ?></td>
                                        </tr>
                                    </tbody>
                                </table>
            </div>
        </div> -->