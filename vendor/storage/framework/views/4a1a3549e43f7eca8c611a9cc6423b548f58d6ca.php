
<table class="uk-table" cellspacing="0" width="100%">
    <thead>
        <th class="uk-width-2-10 uk-text-center">Name</th>
        <th class="uk-width-2-10 uk-text-center">Created</th>
        <th class="uk-width-2-10 uk-text-center">Updated</th>
        <th colspan="2" class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($datas as $data): ?>
        <tr>
            <td class="uk-text-center"><?php echo $data->name; ?></td>
            <td class="uk-text-center"><?php echo $data->created_at; ?></td>
            <td class="uk-text-center"><?php echo $data->updated_at; ?></td>
            <td class="uk-text-right">
                <a title="Edit" href="<?php echo route($location.'.'.$view.'.edit', [$data->id]); ?>"><i class="md-icon material-icons">&#xE254;</i></a>
                <a title="Show" href="<?php echo route($location.'.'.$view.'.show', [$data->id]); ?>"><i class="md-icon material-icons">&#xE88F;</i></a>
            </td>
            <td class="">
                <?php echo Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']); ?>

                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                <?php echo Form::close(); ?>

            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>

<?php echo e($datas->links()); ?>