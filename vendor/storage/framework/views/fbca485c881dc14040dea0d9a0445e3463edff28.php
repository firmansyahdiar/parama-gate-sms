<?php $__env->startSection('content'); ?>


    <div id="page_content">
        <div id="page_content_inner">

            <h4 class="heading_a uk-margin-bottom"><?php echo e($page_title); ?></h4>

            <?php if(count($errors) > 0): ?>
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        <?php foreach($errors->all() as $error): ?>
                            <li><?php echo e($error); ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>

            <div class="md-card">
                <div class="md-card-content">

                      <?php echo Form::model($data, ['url' => [$location.'/'.$view.'/update_profile'.'/'.$data->id], 'method' => 'patch']); ?>

                           <?php echo $__env->make($location.'.'.$view.'.profile_fields', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                      <?php echo Form::close(); ?>


                </div>
            </div>

        </div>
    </div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make($location.'.layouts.layouts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>