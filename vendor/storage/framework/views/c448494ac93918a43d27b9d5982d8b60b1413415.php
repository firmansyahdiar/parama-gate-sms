<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Report.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<table class="table table-striped">
  <thead>
	<tr>
	  <th>Phone</th>
	  <th>Message</th>
	  <th>Masking ID</th>
	  <th>Sent Date</th>
	  <th>Sent Status</th>
	  <th>SMS Length</th>
	</tr>
  </thead>
  <tbody>
	<?php foreach($datas as $data): ?>
	<tr>
		<td><?php echo $data->phone_number; ?></td>
		<td><?php echo $data->message; ?></td>
		<td><?php echo $data->masking->masking; ?></td>
		<td><?php echo $data->sent_date; ?></td>
		<td><?php echo $report_status[ $data->report_status ]; ?></td>
		<td><?php echo ceil(count($data->message)/153); ?></td>
	</tr>
	<?php endforeach; ?>
  </tbody>
</table>