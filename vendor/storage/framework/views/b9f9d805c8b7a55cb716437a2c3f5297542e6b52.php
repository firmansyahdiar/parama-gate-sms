    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
				<?php echo Form::label('masking_id', 'Masking :'); ?>

                <?php echo Form::select('masking_id', $listMaskings, @$data->masking_id, ['class' => 'data-md-selectize']); ?>

            </div>
				<?php echo Form::label('phone_number', 'Phone Number:'); ?>

            <div class="uk-form-row">
                <?php echo Form::text('phone_number', null, ['class' => 'md-input']); ?>

            </div>
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="<?php echo route('frontend.'.$view.'.index'); ?>"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>