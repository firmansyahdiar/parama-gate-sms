// <?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(MainTest::class);
    }
}

class MainTest extends seeder
{
	public function run()
	{

			DB::table('admins')->insert([
					'username' => 'administrator',
					'email' => 'admin@paramateknologi.com',
					'password' => bcrypt(123456)
				]);
			
			DB::table('menus')->insert([
					'name' 	=> 'HOME',
					'route' => 'frontend.dashboard.index',
					'icon' => '&#xE88A;',
					'sort' => 0,
					'parent_id' => 0
				]);
			DB::table('menus')->insert([
					'name' 	=> 'UPLOAD',
					'route' => 'frontend.file_histories.index',
					'icon' => '&#xE864;',
					'sort' => 1,
					'parent_id' => 1
				]);
			DB::table('menus')->insert([
					'name' 	=> 'SETTING',
					'route' => '',
					'icon' => '&#xE8B8;',
					'sort' => 2,
					'parent_id' => 2
				]);
			DB::table('menus')->insert([
					'name' 	=> 'REPORT',
					'route' => '',
					'icon' => '&#xE8AD;',
					'sort' => 3,
					'parent_id' => 3
				]);
			DB::table('menus')->insert([
					'name' 	=> 'ID Masking',
					'route' => 'frontend.maskings.index',
					'icon' => '',
					'sort' => 0,
					'parent_id' => 4
				]);
			DB::table('menus')->insert([
					'name' 	=> 'User',
					'route' => 'frontend.users.index',
					'icon' => '',
					'sort' => 1,
					'parent_id' => 4
				]);
			DB::table('menus')->insert([
					'name' 	=> 'Roles',
					'route' => 'frontend.roles.index',
					'icon' => '',
					'sort' => 2,
					'parent_id' => 5
				]);
			DB::table('menus')->insert([
					'name' 	=> 'Delivery',
					'route' => 'frontend.reportmasking.index',
					'icon' => '',
					'sort' => 0,
					'parent_id' => 6
				]);
			DB::table('menus')->insert([
					'name' 	=> 'Unsent',
					'route' => 'frontend.unsents.index',
					'icon' => '',
					'sort' => 1,
					'parent_id' => 7
				]);
			DB::table('prefixs')->insert([
					'name' 	=> 'Telkomsel'
				]);
			DB::table('settings')->insert([
					'slug' 	=> 'shared_folder',
					'key' 	=> 'Shared Folder',
					'value'	=> '/ftpsms'
				]);
			DB::table('settings')->insert([
					'slug' 	=> 'reset_app',
					'key' 	=> 'Reset App',
					'value'	=> '/ftpsms'
				]);
			DB::table('settings')->insert([
					'slug' 	=> 'reset_delivery',
					'key' 	=> 'Reset Delivery',
					'value'	=> '/ftpsms'
				]);
		$this->command->info('admin added');
	}
}