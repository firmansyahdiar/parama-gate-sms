
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}												
</style>
<script src="{{url('assets')}}/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>

<table class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">User Name</th>
        <th class="uk-width-1-10 uk-text-center">Password</th>
        <th class="uk-width-2-10 uk-text-center">Masking</th>
		<th class="uk-width-1-10 uk-text-center">Quota</th>
        <th class="uk-width-1-10 uk-text-center">Medium</th>
        <th class="uk-width-1-10 uk-text-center">Critical</th>
        <th class="uk-width-1-10 uk-text-center">Action</th>
        <th class="uk-width-2-10 uk-text-center">Balance</th>
    </tr>
    </thead>
    <tbody>
    <?php
		$num = 0;
		$critical ="";
		$medium ="";
		$critical2 ="";
		$medium2 ="";
	?>
    @foreach ($datas as $data)
        <tr>
		<?php
		
		$url = url('api/sendings/get_balance', $data->masking);
		$json = @file_get_contents($url);
		$balance = json_decode($json, true);
		$num ++;
		?>
            <td class="uk-text-center">{{ $data->username }}</td>
            <td class="uk-text-center">{{ $data->password }}</td>
            <td class="uk-text-center">{{ $data->masking }}</td>
			<td class="uk-text-center">{{ $data->quota }}</td>
            <td class="uk-text-center">{{ $data->medium_notif }}</td>
            <td class="uk-text-center">{{ $data->critical_notif }}</td>
            <td class="uk-width-2-10 uk-text-center">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                <a title="Edit" href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td>
										<?php 
											if (($data->quota*$data->critical_notif)/100 >= $balance) {
												$color="color:red;";
												$kedip="berkedip";
												$critical=strtoupper($data->masking);
												if ($critical2){
													$critical2=$critical2.", ".$critical;
												}else{
													$critical2=$critical;
												}
											}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
												$color="color:blue;";
												$kedip="berkedip";
												$medium=strtoupper($data->masking);
												if ($medium2){
													$medium2=$medium2.", ".$medium;
												}else{
													$medium2=$medium;
												}
											}else{
												$color="";
												$kedip="";
											}
										?> 
            <td class="uk-text-right">
										<span class="{{ $kedip }}" style="{{ $color }}">
											 {{ ceil($balance) }} 
										</span>
			</td>
        </tr>
    @endforeach
    </tbody>
</table>

	<button id="critical" name="critical" class="md-btn" data-message="YOUR MASKING <b>{{ $critical2 }}</b> IS CRITICAL ALERT " data-status="danger" data-pos="bottom-right">Danger</button>
	<button id="medium" name="medium" class="md-btn" data-message="YOUR MASKING <b>{{ $medium2 }}</b> IS MEDIUM ALERT " data-status="info" data-pos="bottom-right">Dalanjnger</button>
	<script>	
	$('#critical').hide();
	$('#medium').hide();
	
	<?php
		if ($critical2 || $medium2) {
	?>
		setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
	<?php
		}
		if ($critical2){
	?>
		setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
	<?php
		}
		if ($medium2){
	?>
		setTimeout(function(){ document.getElementById("medium").click(); }, 1000);
	<?php
		}
	?>
	</script>

{{ $datas->links() }}