@extends($location.'.layouts.layouts')

@section('content')


    <div id="page_content">
        <div id="page_content_inner">

            <h4 class="heading_a uk-margin-bottom">{{$page_title}}</h4>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="md-card">
                <div class="md-card-content">

                {!! Form::open(['route' => $location.'.'.$view.'.store']) !!}
                    @include($location.'.'.$view.'.fields')
                {!! Form::close() !!}

                </div>
            </div>

        </div>
    </div>

@endsection
