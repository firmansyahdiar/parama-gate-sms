<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Report.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<table class="table table-striped">
  <thead>
	<tr>
	  <th>Phone Number</th>
	  <th>Message</th>
	  <th>Schedule</th>
	  <th>Masking ID</th>
	  <th>Username</th>
	  <th>Sent Date</th>
	  <th>Sent Status</th>
	  <th>Reason</th>
	  <th>SMS Length</th>
	</tr>
  </thead>
  <tbody>
	@foreach ($datas as $data)
	<tr>
		<td>{!! $data->phone_number !!}</td>
		<td>{!! $data->message !!}</td>
		<td>
		@if($data->schedule != '1900-01-01 00:00:00')
			{!! $data->schedule !!}
		@endif
		</td>
		<td>{!! @$data->masking->masking !!}</td>
		<td>{!! @$data->user->username !!}</td>
		<td>{!! $data->sent_date !!}</td>
		<td>{!! $report_status[ $data->report_status ] !!}</td>
		<td>
			<?php 
				if($data->report_status == 0 ) echo "Waiting";
				if($data->report_status == 1) echo "Waiting";
				if($data->report_status == 2) echo "Success";
				if($data->report_status == 3) echo $data->log;  
			?>
		</td>
		<td>{!! ceil(count($data->message)/153) !!}</td>
	</tr>
	@endforeach
  </tbody>
</table>