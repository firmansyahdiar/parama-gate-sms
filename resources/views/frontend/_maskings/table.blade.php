
<table class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">User Name</th>
        <th class="uk-width-2-10 uk-text-center">Password</th>
        <th class="uk-width-2-10 uk-text-center">Masking</th>
        <th class="uk-width-2-10 uk-text-center">Create</th>
        <th class="uk-width-2-10 uk-text-center">Update</th>
        <!-- <th class="uk-width-2-10 uk-text-center">Action</th> -->
    </tr>
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
            <td class="uk-text-center">{{ $data->username }}</td>
            <td class="uk-text-center">{{ $data->password }}</td>
            <td class="uk-text-center">{{ $data->masking }}</td>
            <td class="uk-text-center">{{ $data->created_at }}</td>
            <td class="uk-text-center">{{ $data->updated_at }}</td>
            <!-- <td class="uk-width-2-10 uk-text-center">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                <a href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                    <button class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td> -->
        </tr>
    @endforeach
    </tbody>
</table>
