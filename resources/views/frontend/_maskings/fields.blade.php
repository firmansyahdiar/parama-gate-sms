
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
            <div class="uk-form-row">
                    {!! Form::label('username', 'Username:') !!}
                    {!! Form::text('username', null, ['class' => 'md-input']) !!}
            </div>
            <div class="uk-form-row">
                    {!! Form::label('password', 'Password:') !!}
                    {!! Form::text('password', null, ['class' => 'md-input']) !!}
            </div>
            <div class="uk-form-row">
                    {!! Form::label('masking', 'Masking:') !!}
                    {!! Form::text('masking', null, ['class' => 'md-input']) !!}
            </div>
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="{!! route('frontend.'.$view.'.index') !!}"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
