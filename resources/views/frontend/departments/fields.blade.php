
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-2">
				{!! Form::label('name', 'Name:') !!}
            <div class="uk-form-row">
				{!! Form::text('name', null, ['class' => 'md-input']) !!}
            </div>
        </div>
    </div>
    <div class="uk-text-right">
        <a class="md-btn md-btn-flat uk-modal-close" href="{!! route($location.'.'.$view.'.index') !!}"> Cancel</a>
        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary" id="snippet_new_save"> Save </button>
    </div>
