


<table>
	<!-- <tr>
		<td width="100"> {!! Form::label('id', 'ID') !!} </td>
		<td> : </td>
		<td> {!! $data->id !!} </td>
	</tr> -->
	<tr>
		<td> {!! Form::label('name', 'Name') !!} </td>
		<td> : </td>
		<td> {!! $data->name !!}  </td>
	</tr>
	<tr>
		<td> {!! Form::label('created_at', 'Created At ') !!} </td>
		<td> : </td>
		<td> {!! $data->created_at !!}  </td>
	</tr>
	<tr>
		<td> {!! Form::label('updated_at', 'Updated At ') !!} </td>
		<td> : </td>
		<td> {!! $data->updated_at !!}  </td>
	</tr>
</table>

<hr><h3 class="heading_a">Menu</h3><hr>
  
{!! Form::open(['url' => 'frontend/roles/role_menus/'.$data->id]) !!}
  <!-- Position Field -->
  
<style>
div.ex1 {
    margin-left: 40px;
}
</style>
	<table>
		@foreach($listMenus as $key)
		<tr>
			<td width="300" class="ex1"> 
				@if($key->parent_id == "0")
					<b>{!! $key->name !!} </b>
				@else
					<div class="ex1"> {!! $key->name !!}  </div>
				@endif
			</td>
			<td> 
				@if($key->route != " ")
					@if(!empty($menus[$key->id]))
						<!-- {!! Form::checkbox('menu_id[]', $key,true) !!} -->
						{!! Form::checkbox('menu_id[]', $key->id, ['class' => '']) !!}
					@else
						{!! Form::checkbox('menu_id[]', $key->id) !!}
					@endif
				@else
					{!! '' !!}
				@endif
			</td>
		</tr>
		@endforeach
	</table>
  <br>
  <div class="uk-text-right">
      <a href="{!! route('frontend.'.$view.'.index') !!}" class="btn btn-default">CANCEL</a>
      {!! Form::submit('Save', ['class' => 'md-btn md-btn-flat md-btn-flat-primary']) !!}
  {!! Form::close() !!}
  </div>