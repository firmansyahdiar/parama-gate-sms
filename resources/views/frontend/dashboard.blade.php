@extends($location.'.layouts.dashboard')

@section('content')

<script src="{{url('assets')}}/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>
<br><br>
    <div id="page_content">
        <div id="page_content_inner">
          <!-- tasks -->
            <div class="uk-grid">
                <div class="uk-width-1-1">
				
					<div class="uk-width-medium-1-1">
						<div class="md-card">
							<div class="md-card-toolbar">
								<div class="md-card-toolbar-actions">
									<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
									<!-- <i class="md-icon material-icons md-card-toggle">&#xE316;</i> -->
									<i class="md-icon material-icons md-card-close">&#xE14C;</i>
								</div>
									<h3 class="md-card-toolbar-heading-text">
										BALANCE
									</h3>
							</div>
							<div class="md-card-content">
								<div class="uk-grid uk-grid-width-large-1-5 uk-grid-width-medium-1-5 uk-grid-medium uk-sortable sortable-handler hierarchical_show" data-uk-sortable data-uk-grid-margin>
								<?php
								$num = 0;
								$critical2 ="";
								$medium2 ="";
								?>
								@foreach ($masking as $data)
								<?php
								$url = url('/api/sendings/get_balance', $data->masking);
								$json = @file_get_contents($url);
								$balance = json_decode($json, true);
								$num ++
								?>
								<div>
									<div class="md-card">
										<div class="md-card-content" style="Background: #DCDCDC;">
											<div class="uk-float-right uk-margin-top uk-margin-small-right"></div>
											<span class="uk-text-muted uk-text-small"><font color="#000000;"> {{ strtoupper($data->masking) }} </font></span>
											<h2 class="uk-margin-remove">&nbsp
												<?php 
													if (($data->quota*$data->critical_notif)/100 >= $balance) {
														$color="color:red;";
														$kedip="berkedip";
														
														$critical=strtoupper($data->masking);
														if ($critical2){
															$critical2=$critical2.", ".$critical;
														}else{
															$critical2=$critical;
														}
												?>
												<?php
													}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
														$color="color:blue;";
														$kedip="berkedip";
														
														$medium=strtoupper($data->masking);
														if ($medium2){
															$medium2=$medium2.", ".$medium;
														}else{
															$medium2=$medium;
														}
													}else{
														$color="";
														$kedip="";
													}
												?>
												<span class="countUpMe {{ $kedip }}" style="{{ $color }}">
													 {{ number_format($balance) }} 
												</span>
												
											</h2>
										</div>
									</div>
								</div>
								@endforeach
								
								<button id="critical" name="critical" class="md-btn" data-message="YOUR MASKING <b>{{ $critical2 }}</b> IS CRITICAL ALERT " data-status="danger" data-pos="bottom-right">Danger</button>
								<button id="medium" name="medium" class="md-btn" data-message="YOUR MASKING <b>{{ $medium2 }}</b> IS MEDIUM ALERT " data-status="info" data-pos="bottom-right">Danger</button>
								<script>
										$('#critical').hide();
										$('#medium').hide();
								<?php
								if ($critical2 || $medium2) {
								?>
										setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
								<?php
								}
								if ($critical2){
								?>
										setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
								<?php
								}
								if ($medium2){
								?>
										setTimeout(function(){ document.getElementById("medium").click(); }, 1000);
								<?php
								}
								?>
								</script>
							</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="uk-grid uk-grid-width-medium-1-3 uk-sortable sortable-handler" data-uk-grid="{gutter:24}" data-uk-sortable>
				<div class="uk-width-medium-1-2">
					<div class="md-card" style="height: 350px;">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<!-- <i class="md-icon material-icons md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">
									&#xE8EE;
									<div class="uk-dropdown uk-dropdown-large" >
										<ul class="uk-nav">
											<li><a href="#" onclick="div1a()" style="text-align: right; font-size: 12px;">APPS</a></li>
											<li><a href="#" onclick="div1b()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER</a></li>
											<li><a href="#" onclick="div1c()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER DELIVER</a></li>
											<li><a href="#" onclick="div1d()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER FAILED </a></li>
										</ul>
									</div>
								</i> -->
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 id="title1" class="md-card-toolbar-heading-text"> </h3>
						</div>
						<div class="md-card-content" style="height: 80%;">
							<div id="grafik" style="height: 87%;"></div>
							<div id="view1" style="height: 87%;"></div>
							<div class="uk-grid" data-uk-grid-margin>
								<div class="uk-width-medium-2-3">
									<div class="uk-form-row">
										&nbsp; Last Reset Counter : {{ $select_masking->graph_app }}
									</div>
								</div>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row" align="right">
										<a href="{{ url('/frontend/dashboard/reset_app/'.$user_masking[0]) }}" class="user_action_image" title="Reset">
											<i class="material-icons">&#xE3AE;</i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card" style="height: 350px;">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<!-- <i class="md-icon material-icons md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">
									&#xE8EE;
									<div class="uk-dropdown uk-dropdown-large" >
										<ul class="uk-nav">
											<li><a href="#" onclick="div2a()" style="text-align: right; font-size: 12px;">APPS</a></li>
											<li><a href="#" onclick="div2b()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER</a></li>
											<li><a href="#" onclick="div2c()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER DELIVER</a></li>
											<li><a href="#" onclick="div2d()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER FAILED </a></li>
										</ul>
									</div>
								</i> -->
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 id="title2" class="md-card-toolbar-heading-text"> </h3>
						</div>
						<div class="md-card-content" style="height: 80%;">
							<div id="grafik2" style="height: 87%;"></div>
							<div id="view2" style="height: 87%;"></div>
							<div class="uk-grid" data-uk-grid-margin>
								<div class="uk-width-medium-2-3">
									<div class="uk-form-row">
										Last Reset Counter : {{ $select_masking->graph_delivery }}
									</div>
								</div>
								<div class="uk-width-medium-1-3">
									<div class="uk-form-row" align="right">
										<a href="{{ url('/frontend/dashboard/reset_delivery/'.$user_masking[0]) }}" class="user_action_image" title="Reset">
											<i class="material-icons">&#xE3AE;</i>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<!-- <i class="md-icon material-icons md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">
									&#xE8EE;
									<div class="uk-dropdown uk-dropdown-large" >
										<ul class="uk-nav">
											<li><a href="#" onclick="div3a()" style="text-align: right; font-size: 12px;">APPS</a></li>
											<li><a href="#" onclick="div3b()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER</a></li>
											<li><a href="#" onclick="div3c()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER DELIVER</a></li>
											<li><a href="#" onclick="div3d()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER FAILED </a></li>
										</ul>
									</div>
								</i> -->
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 id="title3" class="md-card-toolbar-heading-text"> </h3>
						</div>
						<div class="md-card-content">
							<div id="grafik3" style="height: 200px; widht: 100%;"></div>
							<div id="view3" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<!-- <i class="md-icon material-icons md-card-dropdown" data-uk-dropdown="{pos:'bottom-right'}" aria-haspopup="true" aria-expanded="false">
									&#xE8EE;
									<div class="uk-dropdown uk-dropdown-large" >
										<ul class="uk-nav">
											<li><a href="#" onclick="div4a()" style="text-align: right; font-size: 12px;">APPS</a></li>
											<li><a href="#" onclick="div4b()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER</a></li>
											<li><a href="#" onclick="div4c()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER DELIVER</a></li>
											<li><a href="#" onclick="div4d()" style="text-align: right; font-size: 12px;">CONTENT PROVIDER FAILED </a></li>
										</ul>
									</div>
								</i> -->
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 id="title4" class="md-card-toolbar-heading-text"> </h3>
						</div>
						<div class="md-card-content">
							<div id="grafik4" style="height: 200px; widht: 100%;"></div>
							<div id="view4" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<!-- 
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-toggle">&#xE316;</i> 
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									
								</h3>
						</div>
					
						<div class="md-card md-card-hover">
							<div class="gallery_grid_item md-card-content" align="center">
								<a href="{{url('assets')}}/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
									<img src="{{url('assets')}}/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
								</a>
								<div class="gallery_grid_image_caption">
									<span class="gallery_image_title uk-text-truncate">&nbsp</span>
									<span class="uk-text-muted uk-text-small"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								 <i class="md-icon material-icons md-card-toggle">&#xE316;</i> 
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
							<h3 class="md-card-toolbar-heading-text">
								PREFIX OPERATOR
							</h3>
						</div>
						<div class="md-card-content">
							<div id="prefix" style="height: 200px; widht: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-2">
					<div class="md-card">
						<div class="md-card-toolbar">
							<div class="md-card-toolbar-actions">
								<i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
								<i class="md-icon material-icons md-card-toggle">&#xE316;</i>
								<i class="md-icon material-icons md-card-close">&#xE14C;</i>
							</div>
								<h3 class="md-card-toolbar-heading-text">
									
								</h3>
						</div>
					
						<div class="md-card md-card-hover">
							<div class="gallery_grid_item md-card-content" align="center">
								<a href="{{url('assets')}}/img/login-img.png" data-uk-lightbox="{group:'gallery'}">
									<img src="{{url('assets')}}/img/login-img-2.png" alt="" style="height: 200px; widht: 100%;" align="center">
								</a>
								<div class="gallery_grid_image_caption">
									<span class="gallery_image_title uk-text-truncate">&nbsp</span>
									<span class="uk-text-muted uk-text-small"></span>
								</div>
							</div>
						</div>
					</div>
				</div>
				-->
			</div>

			<!--BUTTON TAMBAH
			<div class="md-fab-wrapper">
                <div class="md-fab md-fab-accent md-fab-sheet">
                    <i class="material-icons">&#xE254;</i>
                    <div class="md-fab-sheet-actions">
                        <a href="{!! route($location.'.'.$view.'.create') !!}" class="md-color-white"><i class="material-icons md-color-white">&#xE872;</i> Clear History Graphic</a>
                    </div>
                </div>
			</div>
			-->
        </div>
    </div>

<script>
	var judul1 = "APPS";
	var judul2 = "CONTENT PROVIDER";
	var judul3 = "CONTENT PROVIDER - DELIVER";
	var judul4 = "CONTENT PROVIDER - FAILED";
		//notifquota();
	
	$(document).ready(function(){
		$('#view1').hide();
		$('#view2').hide();
		$('#view3').hide();
		$('#view4').hide();
			
		document.getElementById("title1").innerHTML = judul1;
		document.getElementById("title2").innerHTML = judul2 ;
		document.getElementById("title3").innerHTML = judul3 ;
		document.getElementById("title4").innerHTML = judul4 ;
	});

	function div1() {
		$('#grafik').hide();
		$('#view1').show();
	}
		function div1a() {
			$('#grafik').show();
			$('#view1').hide();
			document.getElementById("title1").innerHTML = judul1;
		}
		function div1b() {
			div1();
			document.getElementById("view1").innerHTML = document.getElementById("grafik2").innerHTML;
			document.getElementById("title1").innerHTML = judul2;
		}
		function div1c() {
			div1();
			document.getElementById("view1").innerHTML = document.getElementById("grafik3").innerHTML;
			document.getElementById("title1").innerHTML = judul3;
		}
		function div1d() {
			div1();
			document.getElementById("view1").innerHTML = document.getElementById("grafik4").innerHTML;
			document.getElementById("title1").innerHTML = judul4;
		}
		
	function div2() {
		$('#grafik2').hide();
		$('#view2').show();
	}
		function div2a() {
			div2();
			document.getElementById("view2").innerHTML = document.getElementById("grafik").innerHTML;
			document.getElementById("title2").innerHTML = judul1;
		}
		function div2b() {
			$('#grafik2').show();
			$('#view2').hide();
			document.getElementById("title2").innerHTML = judul2;
		}
		function div2c() {
			div2();
			document.getElementById("view2").innerHTML = document.getElementById("grafik3").innerHTML;
			document.getElementById("title2").innerHTML = judul3;
		}
		function div2d() {
			div2();
			document.getElementById("view2").innerHTML = document.getElementById("grafik4").innerHTML;
			document.getElementById("title2").innerHTML = judul4;
		}
		
	function div3() {
		$('#grafik3').hide();
		$('#view3').show();
	}
		function div3a() {
			div3();
			document.getElementById("view3").innerHTML = document.getElementById("grafik").innerHTML;
			document.getElementById("title3").innerHTML = judul1;
		}
		function div3b() {
			div3();
			document.getElementById("view3").innerHTML = document.getElementById("grafik2").innerHTML;
			document.getElementById("title3").innerHTML = judul2;
		}
		function div3c() {
			$('#grafik3').show();
			$('#view3').hide();
			document.getElementById("title3").innerHTML = judul3;
		}
		function div3d() {
			div3();
			document.getElementById("view3").innerHTML = document.getElementById("grafik4").innerHTML;
			document.getElementById("title3").innerHTML = judul4;
		}
		
	function div4() {
		$('#grafik4').hide();
		$('#view4').show();
	}
		function div4a() {
			div4();
			document.getElementById("view4").innerHTML = document.getElementById("grafik").innerHTML;
			document.getElementById("title4").innerHTML = judul1;
		}
		function div4b() {
			div4();
			document.getElementById("view4").innerHTML = document.getElementById("grafik2").innerHTML;
			document.getElementById("title4").innerHTML = judul2;
		}
		function div4c() {
			div4();
			document.getElementById("view4").innerHTML = document.getElementById("grafik3").innerHTML;
			document.getElementById("title4").innerHTML = judul3;
		}
		function div4d() {
			$('#grafik4').show();
			$('#view4').hide();
			document.getElementById("title4").innerHTML = judul4;
		}
</script>
@endsection
