<?php
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=Report.xls");  //File name extension was wrong
header("Expires: 0");
header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
header("Cache-Control: private",false);
?>
<table class="table table-striped">
  <thead>
	<tr>
	  <th>Phone</th>
	  <th>Message</th>
	  <th>Schedule</th>
	  <th>Masking ID</th>
	  <th>Username</th>
	  <th>Log</th>
	  <th>Created At</th>
	</tr>
  </thead>
  <tbody>
	@foreach ($datas as $data)
	<tr>
		<td>{!! $data->phone_number !!}</td>
		<td>{!! $data->message !!}</td>
		<td>
		@if($data->schedule != '1900-01-01 00:00:00')
			{!! $data->schedule !!}
		@else
		
		@endif
		</td>
		<td>{!! @$data->masking->masking !!}</td>
		<td>{!! @$data->user->username !!}</td>
		<td>{!! @$data->log !!}</td>
		<td>{!! $data->created_at !!}</td>
	</tr>
	@endforeach
  </tbody>
</table>
