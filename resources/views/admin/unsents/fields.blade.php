
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-1">
            <div class="uk-form-row">
				<h3 class="heading_a">Filter</h3><hr>
            </div>
{!! Form::open(['route' => $location.'.'.$view.'.store']) !!}
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-4">
								{!! Form::label('message', 'Message') !!}
					<div class="uk-form-row">
								{!! Form::text('message', $message, ['class' => 'md-input'])  !!}
					</div>
				</div>
			</div>
			<div class="uk-grid" data-uk-grid-margin>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
								{!! Form::label('tgl_awal', 'Start Date :') !!}
						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								{!! Form::text('tgl_awal', $tgl_awal, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']) !!}
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-4">
					<div class="uk-form-row">
								{!! Form::label('tgl_akhir', 'End Date :') !!}
						<div class="uk-input-group">
							<span class="uk-input-group-addon"><i class="uk-input-group-icon uk-icon-calendar"></i></span>
								{!! Form::text('tgl_akhir', $tgl_akhir, ['class' => 'md-input', 'data-uk-datepicker' => '{format:"YYYY-MM-DD"}']) !!}
						</div>
					</div>
					<br>
				</div>
			</div>
        </div>
    </div>
    <div class="uk-grid" data-uk-grid-margin>
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-right">
					<button type="submit" class="md-btn md-btn-flat-primary" id="snippet_new_save"> PROCESS </button>
{!! Form::close() !!}
				</div>
            </div>
        </div>
        <div class="uk-width-medium-1-6">
            <div class="uk-form-row">
				<div class="uk-text-left">
						{!! Form::open(['url' => $location.'/unsents/clear_session']) !!}
							{!! Form::submit('RESET', ['class' => 'md-btn md-btn-flat-primary']) !!}
						{!! Form::close() !!}
				</div>
            </div>
        </div>
    </div>

