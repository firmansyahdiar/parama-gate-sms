
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
    <tr>
        <th class="uk-width-2-10 uk-text-center">Phone Number</th>
        <th class="uk-width-2-10 uk-text-center">Message</th>
        <th class="uk-width-2-10 uk-text-center">Log</th>
        <th class="uk-width-2-10 uk-text-center">Created</th>
        <th class="uk-width-2-10 uk-text-center">Action</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
            <td class="uk-text-left">{!! $data->phone_number !!}</td>
            <td class="uk-text-left">{!! substr($data->message,0,20) !!}</td>
            <td class="uk-text-left">{{ $data->log }}</td>
            <td class="uk-text-center">{{ $data->created_at }}</td>
            <td class="uk-text-center">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                    <button title="Delete" class="md-icon" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
