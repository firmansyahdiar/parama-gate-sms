


<table>
	<!-- <tr>
		<td width="100"> {!! Form::label('id', 'ID') !!} </td>
		<td> : </td>
		<td> {!! $data->id !!} </td>
	</tr> -->
	<tr>
		<td> {!! Form::label('name', 'Name') !!} </td>
		<td> : </td>
		<td> {!! $data->name !!}  </td>
	</tr>
	<tr>
		<td> {!! Form::label('created_at', 'Created At ') !!} </td>
		<td> : </td>
		<td> {!! $data->created_at !!}  </td>
	</tr>
	<tr>
		<td> {!! Form::label('updated_at', 'Updated At ') !!} </td>
		<td> : </td>
		<td> {!! $data->updated_at !!}  </td>
	</tr>
</table>

<hr><h3 class="heading_a">Menu</h3><hr>
  
{!! Form::open(['url' => 'admin/roles/role_menus/'.$data->id]) !!}
  <!-- Position Field -->
  
<style>
div.ex1 {
    margin-left: 40px;
}
</style>
	<table>
		@foreach($data_menus as $data)
		<tr>
			
			<td width="300" class="ex1"> 
				<b>{!! @$data['name'] !!} </b>
				@if(!empty($data['child']))
					@foreach($data['child'] as $child)
						<div class="ex1"> {!! @$child->name !!}  </div>
					@endforeach
				@endif
			</td>
			<td> 
				@if($data->route != " ")
					@if(!empty($menus[$data->id]))
						{!! Form::checkbox('menu_id[]', $data->id, ['class' => '']) !!}
					@else
						{!! Form::checkbox('menu_id[]', $data->id) !!}
					@endif
				@else
					{!! '' !!}
				@endif
				@if(!empty($data['child']))
					@foreach($data['child'] as $child)
						@if(!empty($menus[$child->id]))
							{!! Form::checkbox('menu_id[]', $child->id, ['class' => '']) !!}<br>
						@else
							{!! Form::checkbox('menu_id[]', $child->id) !!}<br>
						@endif
					@endforeach
				@endif
			</td>
		</tr>
		@endforeach
	</table>
  <br>
  <div class="uk-text-right">
      <a href="{!! route('admin.'.$view.'.index') !!}" class="btn btn-default">CANCEL</a>
      {!! Form::submit('Save', ['class' => 'md-btn md-btn-flat md-btn-flat-primary']) !!}
  {!! Form::close() !!}
  </div>