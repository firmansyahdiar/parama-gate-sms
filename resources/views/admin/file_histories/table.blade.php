<script>
$(document).ready(function() {
    $('#dt_default').DataTable( {
        scrollY:        '50vh',
        scrollCollapse: true,
        paging:         false
    } );
} );
</script>
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}												
</style>
<script src="{{url('assets')}}/js/jquery-latest.js"></script> 
<script>
setInterval(function(){
     $(".berkedip").toggle();
},300);
</script>
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
        <th class="uk-width-2-10 uk-text-center">Name</th>
        <th class="uk-width-2-10 uk-text-center">Log</th>
        <th class="uk-width-2-10 uk-text-center">Created</th>
        <!-- <th class="uk-width-2-10 uk-text-center">Updated At</th> -->
        <th class="uk-width-2-10 uk-text-center">Actions</th>
    </thead>
    <tbody>
    @foreach($datas as $data)
        <tr>
            <td class="uk-text-center">{!! $data->name !!}</td>
            <td class="uk-text-center">{!! $data->log !!}</td>
            <td class="uk-text-center">{!! $data->created_at !!}</td>
            <!-- <td class="uk-text-center">{!! $data->updated_at !!}</td> -->
            <!--<td class="uk-text-center">
                <a href="{!! route($location.'.'.$view.'.edit', [$data->id]) !!}"><i class="md-icon material-icons">&#xE254;</i></a>
                <a href="{!! route($location.'.'.$view.'.show', [$data->id]) !!}"><i class="md-icon material-icons">&#xE88F;</i></a>
            </td> -->
            <td class="uk-text-center">
                {!! Form::open(['route' => [$location.'.'.$view.'.destroy', $data->id], 'method' => 'delete']) !!}
                    <button class="md-icon" title="Delete" type="button" onclick="UIkit.modal.confirm('Are you sure?', function(){ submit(); });"><i class="md-icon material-icons">&#xE872;</i></button>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>


	<?php
		$num = 0;
		$critical ="";
		$medium ="";
		$critical2 ="";
		$medium2 ="";
	?>
	
	@foreach ($masking as $data)
	
	<?php
		$url = url('/api/sendings/get_balance', $data->masking);
		$json = @file_get_contents($url);
		$balance = json_decode($json, true);
		$num ++;
		
		if (($data->quota*$data->critical_notif)/100 >= $balance) {
			$critical=strtoupper($data->masking);
			if ($critical2){
				$critical2=$critical2.", ".$critical;
			}else{
				$critical2=$critical;
			}
		}elseif (($data->quota*$data->medium_notif)/100 >= $balance) {
			$medium=strtoupper($data->masking);
			if ($medium2){
				$medium2=$medium2.", ".$medium;
			}else{
				$medium2=$medium;
			}
		}else{
			$color="";
			$kedip="";
		}
	?>
	@endforeach
								
	<button id="critical" name="critical" class="md-btn" data-message="YOUR MASKING <b>{{ $critical2 }}</b> IS CRITICAL ALERT " data-status="danger" data-pos="bottom-right">Danger</button>
	<button id="medium" name="medium" class="md-btn" data-message="YOUR MASKING <b>{{ $medium2 }}</b> IS MEDIUM ALERT " data-status="info" data-pos="bottom-right">Dalanjnger</button>
	
	<script>	
	$('#critical').hide();
	$('#medium').hide();
	
	<?php
		if ($critical2 || $medium2) {
	?>
		setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
	<?php
		}
		if ($critical2){
	?>
		setTimeout(function(){ document.getElementById("critical").click(); }, 1000);
	<?php
		}
		if ($medium2){
	?>
		setTimeout(function(){ document.getElementById("medium").click(); }, 1000);
	<?php
		}
	?>
	</script>