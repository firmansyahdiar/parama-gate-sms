
<table id="dt_default" class="uk-table" cellspacing="0" width="100%">
    <thead>
        <th class="uk-width-2-10 uk-text-center">Phone Number</th>
        <th class="uk-width-2-10 uk-text-center">Message</th>
        <th class="uk-width-2-10 uk-text-center">Maskings</th>
        <th class="uk-width-2-10 uk-text-center">Send Date</th>
        <th class="uk-width-2-10 uk-text-center">Status</th>
        <th class="uk-width-2-10 uk-text-center">Reason</th>
        <th class="uk-width-2-10 uk-text-center">SMS Length</th>
    </thead>
    <tbody>
    @foreach ($datas as $data)
        <tr>
            <td class="uk-text-left">{{ $data->phone_number }}</td>
            <td class="uk-text-left">
				<?php 
					if (strlen($data->message) >= 50) {
						echo substr($data->message, 0, 50) ." ...";
					} else {
						echo $data->message;
					}
				?>
			</td>
            <td class="uk-text-center">{{ @$data->masking->masking }}</td>
            <!-- <td class="uk-text-center">{{ $data->schedule }}</td> -->
            <td class="uk-text-center">{{ $data->sent_date }}</td>
            <td class="uk-text-center">
				<?php 
					if ($data->report_status == 0 && $data->sending_status == 0 )
					{ 
						echo "Success Send"; 
					}elseif($data->report_status == 0 )
					{ 
						echo "Queue"; 
					} 
					if ($data->report_status == 1){ echo "Queue"; } 
					if ($data->report_status == 2){ echo "Deliver"; }
					if ($data->report_status == 3){ echo "Failed"; }  
				?>
			</td>
			<td class="uk-text-left">
				<?php 
					if($data->report_status == 0 ) echo "Waiting";
					if($data->report_status == 1) echo "Waiting";
					if($data->report_status == 2) echo "Success";
					if($data->report_status == 3) echo $data->log;  
				?>
			</td>
            <td class="uk-text-center">{!! ceil(strlen($data->message)/153) !!}</td>
            
        </tr>
    @endforeach
    </tbody>
</table>

