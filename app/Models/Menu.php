<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

  public $table = 'menus';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'name',
      'route',
      'icon',
      'parent_id',
      'sort',
	];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'name' => 'string',
      'icon' => 'string',
      'route' => 'string',
      'parent_id' => 'string',
      'sort' => 'integer',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
	return 'Y-m-d H:i:s';
  }
  public function role()
  {
      return $this->belongsToMany('App\Models\Role');
  }
}
