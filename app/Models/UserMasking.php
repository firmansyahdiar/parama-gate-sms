<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserMasking extends Model
{

  public $table = 'user_masking';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'user_id',
      'masking_id',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'user_id' => 'string',
      'masking_id' => 'string',
  ];

  public function masking()
  {

      return $this->belongsTo('App\Models\Masking', 'masking_id' , 'id');

  }

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
