<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FileHistory extends Model
{

  public $table = 'file_histories';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'name','source'
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'name' => 'string',
      'source' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [
    
  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
