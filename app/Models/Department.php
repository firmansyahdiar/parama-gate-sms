<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{

  public $table = 'departments';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'name',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'name' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
}
