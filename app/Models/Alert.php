<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alert extends Model
{

  public $table = 'alerts';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
      'masking_id',
      'phone_number',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'masking_id' => 'string',
      'phone_number' => 'string',
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
  public function masking()
  {

      return $this->belongsTo('App\Models\Masking');

  }
}
