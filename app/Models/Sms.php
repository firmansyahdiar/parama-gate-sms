<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{

  public $table = 'sms';

  const CREATED_AT = 'created_at';
  const UPDATED_AT = 'updated_at';

  public $fillable = [
    'user_id',
    'genereated_key',
    'cp_key',
    'message',
    'phone_number',
    'masking_id',
    'schedule',
    'source',
    'sending_status',
    'report_status',
    'sent_date',
    'status_graph',
    'log',
  ];

  /**
   * The attributes that should be casted to native types.
   *
   * @var array
   */
  protected $casts = [
      'id' => 'integer',
      'user_id' => 'integer',
      'genereated_key' => 'string',
      'cp_key' => 'string',
      'message' => 'string',
      'phone_number' => 'string',
      'masking_id' => 'string',
      'schedule' => 'string',
      'sending_status' => 'string',
      'report_status' => 'string',
      'sent_date' => 'string',
      'log' => 'string',
	  'status_graph' => 'string'
  ];

  /**
   * Validation rules
   *
   * @var array
   */
  public static $rules = [

  ];

  public function masking()
  {

      return $this->belongsTo('App\Models\Masking', 'masking_id' , 'id');

  }

  public function user()
  {

      return $this->belongsTo('App\User', 'user_id' , 'id');

  }
  protected function getDateFormat()
  {
    return 'Y-m-d H:i:s';
  }
  protected static function boot() {
      //parent::boot();
      //static::addGlobalScope(new OrderScope('updated_at', 'desc'));
    }
}
