<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Session;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function login_admin_post(Request $request)
    {
        $admin = auth()->guard('admins');
        if( $admin->attempt(['username' => $request->input('username'),'password'=>$request->input('password')]) )
        {
            return redirect()->intended('admin');
        }
        else
        {
            $message = 'Login Failed';
            return redirect()->to('/admin/login')->with(compact('message'));;
        }
    }
	public function logout()
	{
		$admin = auth()->guard('admins');
		$admin->logout();
		Session::flush();
		return redirect('/admin/login');
	}
}
