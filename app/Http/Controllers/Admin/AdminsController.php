<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Admin;
Use View;
Use Validator;

class AdminsController extends Controller
{

    private $page_title  = 'Admins';
    private $controller  = 'Admins';
    private $model       = 'Admin';
    private $view        = 'admins';
    private $location    = 'admin';


    public function __construct()
    {
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $location   = $this->location;

      View::share(
            compact(
              'listRoles',
              'listDepartments',
              'listMaskings',
              'page_title',
              'controller',
              'model',
              'view',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Admin::paginate(10);
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$input = $request->all();
		$input['api_token']  = str_random(60);
		$input['password']  = bcrypt($input['password']);
		Admin::create($input);
		return redirect()->route($this->location.'.'.$this->view.'.index');
      


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Admin::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Admin::findOrFail($id);
      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data','user_masking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Admin::findOrFail($id);

      $input = $request->all();

      if(!empty($input['password']))$input['password']  = bcrypt($input['password']);
      if(empty($input['password']))unset($input['password']);
	  $data->fill($input)->save();
	  
      return redirect()->route($this->location.'.'.$this->view.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Admin::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
}
