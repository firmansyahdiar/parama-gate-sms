<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Jobs\SendingSms;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Models\Masking;
use App\Models\FileHistory;
use App\Models\Sms;

class ImportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $datas;
    
    public function index()
    {
        
    }
    
    public function import($baseDir)
    {
      // $baseDir  = "/mnt/sms";
      $inputDir = $baseDir.'/input/';
      $prosesDir = $baseDir.'/proses/';
      if ($handle = opendir($inputDir)) {
          while (false !== ($value = readdir($handle)))
          {
            $extension = pathinfo($value, PATHINFO_EXTENSION);
            if($extension == 'csv' or $extension == 'xls' or $extension == 'xlsx')
            {
              $fileName = $value;
              $inputFileDir   = $inputDir.$fileName;
              $prosesFileDir  = $prosesDir.date('YmdHis').$fileName;
              $input['name'] = $fileName;
              FileHistory::Create($input);
              rename ( $inputFileDir , $prosesFileDir );
              Excel::load($prosesFileDir, function($reader) {
                  $this->datas = $reader->all();
              });

              ini_set('max_execution_time', 3600*1000);
              foreach($this->datas as $data)
              {
                $data->email    = 'lukmanfrdas@gmail.com';
                $data->password = 123456;
                $masking = Masking::where('masking',$data->masking_id)->first();

                $json = '{
                  "masking_id"    : "'.$masking->id.'",
                  "email"         : "'.$data->email.'",
                  "password"      : "'.$data->password.'",
                  "phone_number"  : "'.$data->phone_number.'",
                  "schedule"      : "'.$data->schedule.'",
                  "sending_status": "1",
                  "message"       : "'.$data->message.'"
                }';
                $url = url('/sendings');
                $input = json_decode($json,true);
                Sms::create($input);
                // $job = new SendingSms($input);
                // $this->dispatch($job);
              }
            }
          }
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
