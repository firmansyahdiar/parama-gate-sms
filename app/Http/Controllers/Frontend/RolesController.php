<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Menu;
use App\Models\Role;
use App\User;
use App\Models\MenuRole;
Use View;
Use Validator;

class RolesController extends Controller
{
    private $page_title  = 'Roles';
    private $controller  = 'Roles';
    private $model       = 'Role';
    private $view        = 'roles';
    private $location    = 'frontend';
    private $aktif    = 'roles';

    public function __construct()
    {
      $list2  = Menu::pluck('name', 'id');
      $listMenus  = Menu::all();
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $aktif   = $this->aktif;
      $location   = $this->location;
	  
      View::share(
            compact(
              'listMenus',
              'list2',
              'page_title',
              'controller',
              'model',
              'aktif',
              'view',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Role::paginate(10);
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, Role::$rules);

      if ($validation->passes())
      {
          Role::create($input);

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Role::findOrFail($id);
	  $menus = MenuRole::where('role_id', $id)->pluck('id','menu_id');
	  //print_r($menus);
      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data','menus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Role::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Role::findOrFail($id);

      $input = $request->all();

      $data->fill($input)->save();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Role::findOrFail($id);

      $user_role = User::where('role_id',$id)->first();
		if(empty($user_role))
		{  
			$data->delete();
		}
		else
		{
			return redirect()->back()->withErrors(['Error', 'Please Input Masking']);
		}

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }

    public function role_menus(Request $request, $id)
    {
      $input = $request->all();
      MenuRole::where('role_id', $id)->delete();
      foreach($input['menu_id'] as $data)
      {
        $save['role_id'] = $id;
        $save['menu_id'] = $data;
        // print_r($save);
        MenuRole::create($save);
      }
      return redirect()->route($this->location.'.'.$this->view.'.show',[$id]);

    }
}
