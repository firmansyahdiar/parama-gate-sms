<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\FileHistory;
use App\Models\Masking;
use App\Models\Setting;
use App\Models\Sms;
Use View;
Use Validator;
use Excel;

class FileHistoriesController extends Controller
{
    private $page_title  = 'FileHistories';
    private $controller  = 'FileHistories';
    private $model       = 'FileHistory';
    private $view        = 'file_histories';
    private $location    = 'frontend';
    private $aktif    	 = 'upload';


    public function __construct()
    {
      $page_title 	= $this->page_title;
      $controller 	= $this->controller;
      $model      	= $this->model;
      $view       	= $this->view;
      $aktif   		= $this->aktif;
      $location   	= $this->location;
      $masking 		= Masking::All();

      View::share(
            compact(
              'page_title',
              'controller',
              'model',
              'aktif',
              'view',
              'location',
			  'masking'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //$datas = FileHistory::paginate(10);
      $datas = FileHistory::orderBy('id','desc')->take(10)->paginate(100);
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, FileHistory::$rules);
      if($request->file('file')){
          $name = $request->file('file')->getClientOriginalName();
		  $setting = Setting::where('slug','shared_folder')->first();
		  $baseDir  = $setting->value;
          $request->file('file')->move('uploads/input', $name);
          $input['name']    = $name;
          $input['source']  = 'uploads/input';
		  
		  $inputDir = $baseDir.'/input/';
		  $inputFileDir   = $inputDir.$name;
		  rename ( public_path().'/uploads/input/'. $name , $inputFileDir );
          // FileHistory::create($input);
          // $this->import('uploads');
          return redirect()->route($this->location.'.'.$this->view.'.index');
      }
	  else
	  {
          return redirect()->route($this->location.'.'.$this->view.'.index');
		  
	  }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = FileHistory::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = FileHistory::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = FileHistory::findOrFail($id);

      $input = $request->all();

      if ($validation->passes())
      {
          $data->fill($input)->save();

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = FileHistory::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
    public function import($baseDir)
    {
      // $baseDir  = "/mnt/sms";
      $inputDir = $baseDir.'/input/';
      $prosesDir = $baseDir.'/proses/';
      if ($handle = opendir($inputDir)) {
          while (false !== ($value = readdir($handle)))
          {
            $extension = pathinfo($value, PATHINFO_EXTENSION);
            if($extension == 'csv' or $extension == 'xls' or $extension == 'xlsx')
            {
              $fileName = $value;
              $inputFileDir   = $inputDir.$fileName;
              $prosesFileDir  = $prosesDir.date('YmdHis').$fileName;
              $input['name'] = $fileName;
              FileHistory::Create($input);
              rename ( $inputFileDir , $prosesFileDir );
              Excel::load($prosesFileDir, function($reader) {
                  $this->datas = $reader->all();
              });

              ini_set('max_execution_time', 3600*1000);
			  $masking = Masking::pluck('id','masking');
			  foreach($masking as $k=>$v)
			  {
				  $masking[$k] = strtolower($v);
			  }
              foreach($this->datas as $data)
              {
                $data->email    = 'lukmanfrdas@gmail.com';
                $data->password = 123456;
                
                $json = '{
                  "masking_id"    : "'.@$masking[strtolower($data->masking_id)].'",
                  "email"         : "'.$data->email.'",
                  "password"      : "'.$data->password.'",
                  "phone_number"  : "'.$data->phone_number.'",
                  "schedule"      : "'.$data->schedule.'",
                  "sending_status": "1",
                  "message"       : "'.$data->message.'"
                }';
                $url = url('/sendings');
                $input = json_decode($json,true);
                Sms::create($input);
                // $job = new SendingSms($input);
                // $this->dispatch($job);
              }
            }
          } 
      }
    }
}
