<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Masking;
use App\Models\Sms;
Use View;
Use Validator;
use Excel;

class Report_MaskingsController extends Controller
{
    private $page_title  = 'Delivery';
    private $controller  = 'Report_Maskings';
    private $model       = 'sms';
    private $view        = 'report_maskings';
    private $location    = 'frontend';


    public function __construct()
    {
      $pending = Sms::where('report_status', '=', '1')->count();
      $deliver = Sms::where('report_status', '=', '2')->count();
      $failed = Sms::where('report_status', '=', '3')->count();
      //$listUser = ['-'=>'ALL'] + User::lists('name', 'id')->all();
      $listMasking = ['-'=>'ALL'] + Masking::lists('masking', 'id')->all();
      $page_title = $this->page_title;
      $controller = $this->controller;
      $model      = $this->model;
      $view       = $this->view;
      $location   = $this->location;

      View::share(
            compact(
              //'listUser',
			  'listMasking',
              'page_title',
              'controller',
              'model',
              'view',
              'location',
			  'pending',
			  'deliver',
			  'failed'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		// DATA SESSION
			//Membuat Session User
			//if (session('user')) {
			//	$user = session('user');
			//}else{
			//	$user = '-';
			//}
			//Membuat Session Status
			if (session('status')) {
				$status = session('status');
			}else{
				$status = '-';
			}
			//Membuat Session Masking
			if (session('masking')) {
				$masking = session('masking');
			}else{
				$masking = '-';
			}
			//Membuat Session tgl_awal
			if (session('tgl_awal')) {
				$tgl_awal = session('tgl_awal');
			}else{
				$tgl_awal = '';
			}
			//Membuat Session tgl_akhir
			if (session('tgl_akhir')) {
				$tgl_akhir = session('tgl_akhir');
			}else{
				$tgl_akhir = '';
			}
		
		//print_r($status);
		
		//DATA FILTER
			//Mengambil Data Status
			if ($status <> '-'){
				$datas = Sms::where('report_status', $status);
			}else{
				$datas = Sms::where('report_status', '<>', '9');
			}
			//Mengambil Data Tanggal
			if ($tgl_awal <> '' and $tgl_akhir <> ''){
				$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
			}
			//Mengambil Data User
			//if ($user <> '-'){
			//	$datas = $datas->where('user_id', $user);
			//}
			//Mengambil Masking
			if ($masking <> '-') {
				$datas = $datas->where('masking_id', $masking);
			}
		
        $datas = $datas->orderBy('created_at', 'desc')->take(100)->get();
		
        return view($this->location.'.'.$this->view.'.index')
            ->with(compact('datas'))
            ->with(compact('tgl_awal', 'tgl_akhir', 'status', 'masking'));
    }

    public function store(Request $request)
    {
		//DATA INPUT
		  $input = $request->all();
		  //$user = $request->input('user');
		  $status = $request->input('status');
		  $masking = $request->input('masking');
		  $tgl_awal = $request->input('tgl_awal');
		  $tgl_akhir = $request->input('tgl_akhir');
		
		//MASUKAN KE SESSION
		//$request->session()->put('user', $user);
		$request->session()->put('status', $status);
		$request->session()->put('masking', $masking);
		$request->session()->put('tgl_awal', $tgl_awal);
		$request->session()->put('tgl_akhir', $tgl_akhir);
	  
	    //print_r( $request->session()->get('user'));
		
		//DATA FILTER
			//Mengambil Data Status
			if ($status <> '-'){
				$datas = Sms::where('report_status', $status);
			}else{
				$datas = Sms::where('report_status', '<>', '9');
			}
			//Mengambil Data User
			//if ($user <> '-'){
			//	$datas = $datas->where('user_id', $user);
			//}
			//Mengambil Masking
			if ($masking <> '-') {
				$datas = $datas->where('masking_id', $masking);
			}
			//Mengambil Data Tanggal
			if ($tgl_awal <> '' and $tgl_akhir <> ''){
				$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
			}
		
		
		//$datas = $datas->get();
        $datas = $datas->orderBy('created_at', 'desc')->take(100)->get();;
        return view($this->location.'.'.$this->view.'.index')
            ->with(compact('datas'))
            ->with(compact('tgl_awal', 'tgl_akhir', 'status', 'masking'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
	
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
	
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
	
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }
	
	public function clear_session(Request $request)
	{
		//$request->session()->put('user', '-');
		$request->session()->put('status', '-');
		$request->session()->put('masking', '-');
		$request->session()->put('tgl_awal', '');
		$request->session()->put('tgl_akhir', '');
		
		//print_r(session('user'));
        return redirect('admin/report_maskings');
	}
	public function export_xls(Request $request)
	{
		ini_set('max_execution_time', 3600+1000);
		ini_set('memory_limit', '-1');
		$input = $request->all();
	    //$user = session('user');
	    $status = session('status');
		$masking = session('masking');
	    $tgl_awal = session('tgl_awal');
	    $tgl_akhir = session('tgl_akhir');
		
		// exit();
			
		//Mengambil Data Status
		if ($status <> '-'){
			$datas = Sms::where('report_status', $status);
		}else{
			$datas = Sms::where('report_status', '<>', '9');
		}
		//Mengambil Data Tanggal
		if ($tgl_awal <> '' and $tgl_akhir <> ''){
			$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
		}
		//Mengambil Data User
		//if ($user <> '-'){
		//	$datas = $datas->where('user_id', $user);
		//}
		//Mengambil Masking
		if ($masking <> '-') {
			$datas = $datas->where('masking_id', $masking);
		}
		$datas = $datas->orderBy('created_at', 'desc')->get();
		// print_r($datas);
		
		return view($this->location.'.'.$this->view.'.bigdump')
            ->with(compact('datas'));
	}
	public function export_csv(Request $request)
	{
		$input = $request->all();
	    //$user = session('user');
	    $status = session('status');
		$masking = session('masking');
	    $tgl_awal = session('tgl_awal');
	    $tgl_akhir = session('tgl_akhir');
		//print_r($input);
			
		//Mengambil Data Status
		if ($status <> '-'){
			$datas = Sms::where('report_status', $status);
		}else{
			$datas = Sms::where('report_status', '<>', '9');
		}
		//Mengambil Data Tanggal
		if ($tgl_awal <> '' and $tgl_akhir <> ''){
			$datas = $datas->whereBetween('sent_date', [$tgl_awal.' 00:00:00', $tgl_akhir.' 23:59:59']);
		}
		//Mengambil Data User
		//if ($user <> '-'){
		//	$datas = $datas->where('user_id', $user);
		//}
		//Mengambil Masking
		if ($masking <> '-') {
			$datas = $datas->where('masking_id', $masking);
		}

		Excel::create('Filename', function($excel) use($datas) {

			$excel->sheet('Sheetname', function($sheet) use($datasw) {
				
				
				$datasw = $datasw->get();
				
				// Manipulate first row
				$sheet->row(1, array('REPORT MASKINGS' ));
				$sheet->row(2, array('Username', 'Phone Number', 'Message', 'Masking', 'Schedule', 'Send Date' ));

				$no = 3;
				foreach ($datasw as $data){
					$sheet->row($no, array(
						@$data->user->name, $data->phone_number, $data->message, @$data->masking->masking, $data->schedule, $data->send_date
					));
					$no = $no + 1 ;
				}

			});

		})->export('csv');

		return Redirect::back();
	}
	
}
