<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Alert;
use App\Models\Masking;
Use View;
Use Validator;

class AlertsController extends Controller
{
    private $page_title  = 'Alerts';
    private $controller  = 'Alerts';
    private $model       = 'Alert';
    private $view        = 'alerts';
    private $location    = 'frontend';
    private $aktif    = 'alerts';


    public function __construct()
    {
	  $listMaskings = Masking::pluck('masking','id');
      $page_title 	= $this->page_title;
      $controller 	= $this->controller;
      $model      	= $this->model;
      $view       	= $this->view;
      $location   	= $this->location;
      $aktif   	= $this->aktif;
      View::share(
            compact(
              'listMaskings',
              'page_title',
              'controller',
              'model',
              'view',
              'aktif',
              'location'
            )
          );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $datas = Alert::all();
      return view($this->location.'.'.$this->view.'.index')
          ->with(compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view($this->location.'.'.$this->view.'.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $validation = Validator::make($input, Alert::$rules);

      if ($validation->passes())
      {
          Alert::create($input);

          return redirect()->route($this->location.'.'.$this->view.'.index');
      }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data = Alert::findOrFail($id);

      return view($this->location.'.'.$this->view.'.show')
                ->with(compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data = Alert::findOrFail($id);

      return view($this->location.'.'.$this->view.'.edit')
                  ->with(compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $data = Alert::findOrFail($id);

      $input = $request->all();

      $data->fill($input)->save();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = Alert::findOrFail($id);

      $data->delete();

      return redirect()->route($this->location.'.'.$this->view.'.index');

    }
}
