<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Jobs\SendingSms;
use App\User;
use App\Admin;
use App\Models\Masking;
use App\Models\Sms;
use App\Models\Alert;
use Artisan;
use Response;
use Hash;
use Auth;

class SendingsController extends Controller
{
    public function index(Request $request)
    {
      $input['schedule']      = $request->schedule;
      $input['message']       = $request->message;
      $input['phone_number']  = $request->phone_number;
	  $input['sent_date'] 	  = date('Y-m-d H:i:s');
      $web = Auth::guard('web');
      if( $web->attempt(['username' => $request->input('username'),'password'=>$request->input('password')]) )
      {
        $input['user_id']       = Auth::user()->id;
        // cek masking_id
        $masking = Masking::where('masking',$request->masking_id)->orWhere('id',intval($request->masking_id))->first();
		$check_format = $this->check_format($input['phone_number']);
		if($check_format['status'] == true)
        {
			if(!empty($masking))
			{
			  
				$input['id']            = $request->id;
				$input['masking_id']    = $masking->id;
				$url_data = 'http://103.16.199.187/masking/send.php?';
				$url_data .=  'username='.$masking->username;
				$url_data .=  '&password='.$masking->password;
				$url_data .=  '&hp='.$this->format_phone_number($request->phone_number);
				$url_data .=  '&message='.urlencode($request->message);
				$id = @file_get_contents($url_data);
				$input['sending_status']= 11;
			  
				$input['generated_key'] = bcrypt(date('YmdHis'));
				if(abs($id)>0)
				{
				  $input['cp_key']        = $id; 
				  $input['log']           = $id;
				  $input['sending_status']= 0;
				  $this->save_sms($input);
				  return Response::json(json_encode($input).', berhasil',201);
				}
				else
				{
				  $input['log']           = $id;
				  $input['sending_status']= 0;
				  $this->save_sms($input);
				  return Response::json($id.', move to unsent',500);
				}
			  
			}
			else
			{
			  return Response::json("error saving, masking not found",500);
			}
		}
		else
		{
			return Response::json("error saving, ".$check_format['message'],500);
		}
      }
      else
      {
        return Response::json("error saving, wrong username or password",500);
      }
	  
    }

    public function status($sending_code)
    {
		$data = Sms::where('cp_key',$sending_code)->first();
		$id = file_get_contents('http://103.16.199.187/masking/report.php?rpt='.$sending_code);
		if(!$id)
		{
			return Response::json("error saving",500);
		}
		if(!empty($id))
		{
			$code = explode(',',$id);
			$input['id']              = $data->id;
			$input['cp_key']          = $data->cp_key;
			$input['generated_key']   = $data->generated_key;
			if($code[0] == 50)
			{
			  $input['report_status'] = 3;
			}
			elseif($code[0] == 22)
			{
			  $input['report_status'] = 2;
			}
			elseif($code[0] == 20)
			{
			  $input['report_status'] = 1;
			}
			$this->save_sms($input);
			$report[] = $input;
		}
		return Response::json($id,200);
    }
    public function set_all_status()
    {
      $datas = Sms::where('report_status' , '<' , 2)
                    ->where('cp_key' , '<>' , NULL)
                    ->orWhere('report_status', NULL)
                    ->orderBy('id', 'desc')
                    ->paginate(100);
					
      $report = '';
      foreach($datas as $data)
      {
        $id = @file_get_contents('http://103.16.199.187/masking/report.php?rpt='.$data->cp_key);
		if(!empty($id))
		{
			$code = explode(',',$id);
			$input['id']              = $data->id;
			$input['cp_key']          = $data->cp_key;
			$input['generated_key']   = $data->generated_key;
			if($code[0] == 50)
			{
			  $input['report_status'] = 3;
			}
			elseif($code[0] == 22)
			{
			  $input['report_status'] = 2;
			}
			elseif($code[0] == 20)
			{
			  $input['report_status'] = 1;
			}
			$this->save_sms($input);
			$report[] = $input;
		}
      }
      return Response::json($report,200);
    }
	public function send_failed()
	{
		ini_set('max_execution_time', 3600+1000);
		ini_set('memory_limit', '-1');
		
		$count_send = Sms::where('sending_status',1)
					->orWhere([/*['masking_id','<>',0],*/'cp_key'=>null,'sending_status'=>0])
					->whereDate('schedule', '<=', date('Y-m-d H:i:s'))
					->count();
		$count_queue= Sms::where('sending_status',10)
					->whereDate('schedule', '<=', date('Y-m-d H:i:s'))
					->count();
		if($count_send == 0 and $count_queue == 0)
		{
			$sms = Sms::where('sending_status',12)
					->orWhere(['sending_status'=>11])
					->orderBy('id','asc')
					->paginate(250);
			$this->send($sms);
			
		}
	}
	
	public function send_paginate()
	{
		ini_set('max_execution_time', 3600+1000);
		ini_set('memory_limit', '-1');
		// $count_send = Sms::where('sending_status',10)->count();
		// if( $count_send <= 14000 )
		// {
			$sms = Sms::where('schedule', '<=', date('Y-m-d H:i:s'))
					->where(['sending_status'=>1])
					->orWhere(['sending_status'=>0,'cp_key'=>null])
					->orderBy('id','asc')
					->limit(1000)
					->get();
			print_r($sms);
			$this->send($sms);	 
		// }
	}
	public function sent_asc()
	{
		$sms = Sms::orWhere(['sending_status',1,'cp_key'=>null,'sending_status'=>0])
					->where('schedule', '<=', "'".date('Y-m-d H:i:s')."'")
					->orderBy('id','asc')
					->limit(1000)
					->get();
		print_r($sms);
		$this->send($sms);
	}
	public function sent_desc()
	{
		sleep(5);
		$sms = Sms::where('sending_status',1)
					->orWhere([/*['masking_id','<>',0],*/'cp_key'=>null,'sending_status'=>0])
					->whereDate('schedule', '<=', date('Y-m-d H:i:s'))
					->orderBy('id','desc')
					->limit(1000)
					->get();
		print_r($sms);
		$this->send($sms);
	}
	function send($sms)
	{
		foreach($sms as $data)
		{
			$input['id']          = $data->id;
			$input['sending_status']= 10;
			$this->save_sms($input);
		}
		foreach($sms as $data)
		{
			$input['id']          = $data->id;
			$input['email']       = 'lukmanfrdas@gmail.com';
			$input['password']    = 123456;
			$input['phone_number']= $data->phone_number;
			$input['message']     = $data->message;
			$input['schedule']    = $data->schedule;
			$input['masking_id']  = $data->masking_id;
			$input['user_id']  = $data->user_id;
			// print_r($input);
			echo $this->send_api($input);
		}
	}
	public function send_api($request)
	{
		ini_set('memory_limit', '-1');
		
		$input['id']            = @$request['id'];
		$input['schedule']      = @$request['schedule'];
		$input['message']       = @$request['message'];
		$input['phone_number']  = @$request['phone_number'];
		$input['user_id']  		= @$request['user_id'];
		$input['sent_date'] = date('Y-m-d H:i:s');
		$input['generated_key'] = date('YmdHis');
        $masking = Masking::where('masking',$request['masking_id'])->orWhere('id',intval($request['masking_id']))->first();
		
		if( !empty($input['user_id']) )
		{
			if($this->check_format($input['phone_number']) == true)
			{
				if(!empty($masking))
				{
					$input['id']            = @$request['id'];
					$input['masking_id']    = $masking->id;
				  
					$url_data = 'http://103.16.199.187/masking/send.php?';
					$url_data .=  'username='.$masking->username;
					$url_data .=  '&password='.$masking->password;
					$url_data .=  '&hp='.$this->format_phone_number($request['phone_number']);
					echo $url_data .=  '&message='.urlencode($request['message']);
					ini_set('default_socket_timeout', 900);
				 
					echo $id = @file_get_contents($url_data);
				  
					$input['generated_key'] = bcrypt(date('YmdHis'));
					if(abs($id)>0)
					{
					  $input['cp_key']        = $id; 
					  $input['log']           = $id;
					  $input['sending_status']= 0;
					  $this->save_sms($input);
					  return Response::json(json_encode($input).', berhasil',201);
					}
					else
					{
					  $input['log']           = $id;
					  $input['sending_status']= 0;
					  $this->save_sms($input);
					  return Response::json($id.', move to unsent',500);
					}
				}
				else
				{
					$this->save_sms($input);
					return Response::json("error saving, masking not found, move to unsent",500);
				}
			}
			else
			{
				$input['user_id'] = 0;
				$this->save_sms($input);
				return Response::json("error saving, invalid format, move to unsent",500);
			}
		}
		else
		{
			$this->save_sms($input);
			return Response::json("error saving, username not found, move to unsent",500);
		}
	}
	public function send_api2($request)
    {
	  ini_set('max_execution_time', 3600+1000);
	  //API Url
	  // $url = "http://lms.paramateknologi.local/api/sendings";
	  $url = "http://smjkt-dvsms01/api/sendings";
	  $url = url("/api/sendings");
	  //echo $url;exit();
	  //Initiate cURL.
	  $ch = curl_init($url);


	  //Encode the array into JSON.
	  $jsonDataEncoded = json_encode($request);

	  //Tell cURL that we want to send a POST request.
	  curl_setopt($ch, CURLOPT_POST, 1);

	  //Attach our encoded JSON string to the POST fields.
	  curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

	  //Set the content type to application/json
	  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

	  //Execute the request
	  $result = curl_exec($ch);
    }
    public function queue(Request $request)
    {
      ini_set('max_execution_time', 3600*1000);
      $sms = Sms::where('sending_status',1)
					->orWhere([/*['masking_id','<>',0],*/'cp_key'=>null,'sending_status'=>0])
					->whereDate('schedule', '<=', date('Y-m-d H:i:s'))
					->paginate(500);
	  // print_r($sms);
	  foreach($sms as $data)
      {
        $input['id']          = $data->id;
        $input['email']       = 'lukmanfrdas@gmail.com';
        $input['password']    = 123456;
        $input['phone_number']= $data->phone_number;
        $input['message']     = $data->message;
        $input['schedule']    = $this->get_date($data->schedule);
        $input['masking_id']  = $data->masking_id;
        $input['sending_status']= 10;
        $this->save_sms($input);
        $job = new SendingSms($input);
        $this->dispatch($job);
      }	  
	  
	  // ini_set('max_execution_time', 3600*1000);

      // for($i=0;$i<=1000;$i++)
      // {
      //   Artisan::call('queue:work');
      // }
      // ini_set('max_execution_time', 3600*1000);
      // for($i=0;$i<=100;$i++)
      // {
      //   $request = json_decode('{
      //     "username"      : "parama1",
      //     "password"      : "123456789",
      //     "phone_number"  : "08974",
      //     "message"       : "test'.$i.'"
      //   }');
      //   $job = new SendingSms($request);
      //
      //   $this->dispatch($job);
      // }
    }
	
	public function get_balance($masking)
	{
		$masking = Masking::where('masking',$masking)->first();
		// print_r($masking);
		$balance = @file_get_contents('http://103.16.199.187/masking/balance.php?username='.$masking->username.'&password='.$masking->password); 
		return Response::json($balance,200);
	}
	function check_format($phone_number)
	{
		$data_string = $phone_number;
		$zero = substr($data_string, 0,3);
		$zero2 = substr($data_string, 0,2);
		$cek_eight = substr($data_string, 0,1);
		if( $zero2 == '62' )$data_string = '0'.substr($data_string, 2,strlen($data_string));
		if( $cek_eight == '8' )$data_string = '0'.$data_string;
		if( $zero == '+62' or is_numeric($data_string) == false ) {
			$response['status'] 	= false;
			$response['message'] 	= "Wrong Number, It must be Numeric";
			return $response;
		}
		if( strlen($data_string) > 13 ) {
			$response['status'] 	= false;
			$response['message'] 	= "Error saving, Wrong Number, Number of digit more than 13";
			return $response;
		}
		if( strlen($data_string) < 9 ) {
			$response['status'] 	= false;
			$response['message'] 	= "Error saving, Wrong Number, Number of digit less than 9";
			return $response;
		}
		$zero = substr($data_string, 0,1);
		$zero2 = substr($data_string, 0,2);
		if( $zero == '0' ) {
			$response['status'] 	= true;
			$response['message'] 	= "";
		}
		elseif( $zero == '8' ) {
			$response['status'] 	= true;
			$response['message'] 	= "";
		}
		elseif( $zero2 == '62' ) {
			$response['status'] 	= true;
			$response['message'] 	= "";
		}
		else
		{
			$response['status'] 	= false;
			$response['message'] 	= "Wrong Number, It must be 8,62 or 0";
		}
		return $response;
	}
    function format_phone_number($phone_number)
    {
      $data_string = $phone_number;
      $zero = substr($data_string, 0,1);
      if( $zero == '0' )
      {
        $data_string = substr($data_string, 1, strlen($data_string));
      }
      $zero = substr($data_string, 0,2);
      if( $zero == '62' )
      {
        $data_string = substr($data_string, 2, strlen($data_string));
      }
      $zero = substr($data_string, 0,3);
      if( $zero == '+62' )
      {
        $data_string = substr($data_string, 3, strlen($data_string));
      }
      return $phone_number = "0".$data_string;
    }
    function curl_request_async($link)
    {

      $http_response = "";
      $url = parse_url($link);
      $fp = fsockopen($url["host"], 80, $err_num, $err_msg, 30) or die("Socket-open
      failed--error: ".$err_num." ".$err_msg);
      fputs($fp, "GET ".$url['path']."?".$url['query']." HTTP/1.0\n");
      fputs($fp, "Connection: close\n\n");
      while(!feof($fp)) {
      $http_response = fgets($fp, 128);
      }
      fclose($fp);
      return $http_response;
    }
    function save_sms($input)
    {
      if(!empty($input['id']))
      {
        $data = Sms::findOrFail($input['id']);
        $data->fill($input)->save();
      }
      else
      {
        Sms::create($input);
      }
    }
	function save_masking($input)
	{
		if(!empty($input['id']))
		{
			$data = Masking::findOrFail($input['id']);
			$data->fill($input)->save();
		}
		else
		{
			Masking::create($input);
		}
	}
	function get_date( $date )
	{ 
		// $date = '2016-06-16 12:00:00';
		if (preg_match ("/^([0-9]{2})-([0-9]{2})-([0-9]{2}) ([0-9]{2})$/", $date, $split))	
		{
			$data = '20'.$split[3].'-'.$split[1].'-'.$split[2].' '.$split[4].':00:00';
		}
		elseif (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}:[0-9]{2}:[0-9]{2})$/", $date, $split))	
		{
			$data = $date;
		}
		return $data;
	}
	function curl_url($link)
	{
		$ch =  curl_init($link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_TIMEOUT, 3);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
		$result = curl_exec($ch);
		if($result === false)
		{
			curl_close($ch);
			return false;
		}
		else
		{
			curl_close($ch);
			return $result;
		}
	}
	
	function send_admin($masking,$message)
	{
		$admins = Admin::get();
		foreach($admins as $data)
		{
			$url_data = 'http://103.16.199.187/masking/send.php?';
			$url_data .=  'username='.$masking->username;
			$url_data .=  '&password='.$masking->password;
			$url_data .=  '&hp='.$this->format_phone_number($data->phone_number);
			$url_data .=  '&message='.urlencode($message);
			$id = @file_get_contents($url_data);
		}
	}
	
	function send_masking_alert($masking,$message)
	{
		$alerts = Alert::where('masking_id',$masking->id)->get();
		foreach($alerts as $data)
		{
			$request['masking_id'] 		= $masking->id;
			$request['phone_number'] 	= $data->phone_number;
			$request['message'] 		= $message;
			$request['user_id'] 		= 10000;
			$this->send_api($request);
		} 
	}
	
	public function alert()
	{
		$maskings = Masking::get();
		foreach($maskings as $data)
		{
			$url = url('/api/sendings/get_balance/'. $data->masking);
			$json = @file_get_contents($url);
			if(!empty($json))
			{
			$balance = json_decode($json, true);
			$url = url('/api/sendings/get_balance/'. $data->masking);
			$id 			= $data->id;
			$masking 		= $data->masking;
			$quota 			= $data->quota;
			$last_notif 	= $data->last_notif;
			$medium_sent 	= $data->medium_sent; 
			$medium_notif 	= ($data->medium_notif/100) * $quota;
			$medium_interval= $data->medium_interval;
			$medium_max = $data->medium_max;
			$critical_sent 	= $data->critical_sent;
			$critical_notif = ($data->critical_notif/100) * $quota;
			$critical_interval = $data->critical_interval;
			$critical_max = $data->critical_max;
			$count_time = round((strtotime(date('Y-m-d H:i:s')) - strtotime($last_notif))/60);
			echo '<br>'. $masking .' balance '.$balance.' and average quota is '.$quota.' medium quota is '.$medium_notif.' critical notif is '.$critical_notif. ' waktu terakhir notif '.$count_time.' interval '.$critical_interval .' critical sent '.$critical_sent.' critical max '.$critical_max.' medium notif '.$medium_notif.' medium interval '.$medium_interval.' medium sent '.$medium_sent.' medium interval '.$medium_interval;
			if( $balance >= $quota )
			{
				$masking_input['id'] 		= $id; 
				$masking_input['medium_sent']= 0;
				$masking_input['critical_sent']= 0;
				$this->save_masking($masking_input);
			}
			else
			{
				if( $balance <= $critical_notif )
				{
					if($count_time > $critical_interval )
					{
						//cek sms terkirim
						if( $critical_sent < $critical_max )
						{
							echo $message = 
							'CRITICAL ALERT!!!

Sisa Token Masking ID '. $masking .' Anda sebesar ' . $balance . '. Segera lakukan top up Token Masking ID Anda.
Terima kasih.';
							$masking_input['id'] 		= $id; 
							$masking_input['critical_sent']= $critical_sent+1; 
							$masking_input['last_notif']= date('Y-m-d H:i:s'); 
							$this->save_masking($masking_input);
							$this->send_masking_alert($data,$message);
						}
					}
				}
				elseif( $balance <= $medium_notif )
				{
					$count_time = round((strtotime(date('Y-m-d H:i:s')) - strtotime($last_notif))/60);
					
					if($count_time > $medium_interval )
					{
						//cek sms terkirim
						if( $medium_sent < $medium_max )
						{
							echo $message = 
							'MEDIUM ALERT;

Sisa Token Masking ID '. $masking .' Anda sebesar ' . $balance . '. Segera lakukan top up Token Masking ID Anda.
Terima kasih.';
							$masking_input['id'] 		= $id; 
							$masking_input['medium_sent']= $medium_sent+1; 
							$masking_input['last_notif']= date('Y-m-d H:i:s'); 
							$this->save_masking($masking_input);
							$this->send_masking_alert($data,$message);
						}
					}
				}
			}
			}
		}
	}
}
