  <?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    //return view('welcome');
    return redirect()->to('frontend/dashboard/');
});

Route::get('/admin', function () {
    return redirect()->to('admin/dashboard');
});

Route::get('/admin/logout', ['uses' => 'HomeController@logout']);

Route::post('admin/login','HomeController@login_admin_post');

Route::get('admin/login',function(){
	return view('admin/layouts/login');
});

Route::group(['middleware' => 'admin','prefix' => 'admin'], function(){
  Route::group(['namespace' => 'Admin'], function(){
    Route::resource('dashboard', 'DashboardController');
    Route::resource('maskings', 'MaskingsController');
    Route::resource('prefixs', 'PrefixsController');
    Route::resource('settings', 'SettingsController');
    Route::resource('file_histories', 'FileHistoriesController');
    Route::resource('departments', 'DepartmentsController');
    Route::resource('menus', 'MenusController');
    Route::resource('roles', 'RolesController');
    Route::resource('admins', 'AdminsController');
    Route::resource('unsents', 'UnsentsController');
	Route::post('unsents/export_xls','UnsentsController@export_xls');
	Route::post('unsents/clear_session','UnsentsController@clear_session');
    Route::post('roles/role_menus/{id}',['as'=>'admin.roles.role_menus','uses'=>'RolesController@role_menus']);
    Route::delete('prefixs/delete_prefix_number/{id}',['as'=>'admin.prefixs.delete_prefix_number','uses'=>'PrefixsController@delete_prefix_number']);
    Route::post('prefixs/prefix_number/{id}',['as'=>'admin.prefixs.prefix_number','uses'=>'PrefixsController@prefix_number']);
    Route::resource('users', 'UsersController');
    Route::resource('report_maskings', 'Report_MaskingsController');
    Route::post('report_maskings/export_csv','Report_MaskingsController@export_csv');
    Route::post('report_maskings/export_xls','Report_MaskingsController@export_xls');
    Route::resource('alerts', 'AlertsController');
    Route::post('report_maskings/clear_session','Report_MaskingsController@clear_session');
  });
});

Route::group(['middleware' => ['web','role_menu','menu']], function(){
  Route::group(['namespace' => 'Frontend','prefix' => 'frontend'], function(){
	Route::get('dashboard/reset_app/{id}', ['uses' => 'DashboardController@reset_app']);
	Route::get('dashboard/reset_delivery/{id}', ['uses' => 'DashboardController@reset_delivery']);
    Route::resource('dashboard', 'DashboardController');
    Route::resource('maskings', 'MaskingsController');
    Route::resource('file_histories', 'FileHistoriesController');
    Route::resource('departments', 'DepartmentsController');
    Route::resource('menus', 'MenusController');
    Route::resource('roles', 'RolesController');
    Route::resource('unsents', 'UnsentsController');
    Route::post('roles/role_menus/{id}',['as'=>'frontend.roles.role_menus','uses'=>'RolesController@role_menus']);
    Route::get('users/profile',['as'=>'frontend.users.profile','uses'=>'UsersController@profile']);
    Route::patch('users/update_profile/{id}',['as'=>'frontend.users.update_profile','uses'=>'UsersController@update_profile']);
    
  	Route::resource('users', 'UsersController');
    
    Route::resource('reportmaskings', 'ReportMaskingsController');
    Route::resource('reportmaskings/store', 'ReportMaskingsController@store');
    Route::post('reportmaskings/export_csv','ReportMaskingsController@export_csv');
	Route::post('reportmaskings/export_xls','ReportMaskingsController@export_xls');
	Route::post('unsents/export_xls','UnsentsController@export_xls');
	Route::post('unsents/clear_session','UnsentsController@clear_session');
    Route::resource('alerts', 'AlertsController');
    Route::post('reportmaskings/clear_session','ReportMaskingsController@clear_session');
  });
});
Route::group(['namespace' => 'Api','prefix'=>'api'],function(){
  
  Route::get('imports', ['uses' => 'ImportsController@index']);
  Route::get('sendings/alert', ['uses' => 'SendingsController@alert']);
  Route::get('sendings/queue', ['uses' => 'SendingsController@queue']);
  Route::get('sendings/set_all_status', ['uses' => 'SendingsController@set_all_status']);
  Route::get('sendings/send_paginate', ['uses' => 'SendingsController@send_paginate']);
  Route::get('sendings/send_failed', ['uses' => 'SendingsController@send_failed']); 
  Route::get('sendings/status/{generated_key}', ['uses' => 'SendingsController@status']);
  Route::get('sendings/get_balance/{masking}', ['uses' => 'SendingsController@get_balance']);
  Route::post('sendings', ['uses' => 'SendingsController@index']);

//  Route::resource('users','UsersController',array('except'=>array('create','edit')));
});
//Route::get('/a', function() {
//    return auth()->user();
//});
//Route::get('/api', function() {
//
//    if (auth()->check()) {
//        return auth()->user();
//    }
//
//    abort(403, "You're not authorized to access this page.");
//});
//Route::get('api', function() {
//
//    $auth = auth()->guard('api'); // Switch guard ke "api" driver
//
//    if ($auth->check()) {
//        return $auth->user();
//    };
//
//    abort(403, "You're not authorized to access this public REST API.");
//});
Route::group(['namespace' => 'Api','prefix'=>'api'],function(){
	Route::resource('users','UsersController',array('except'=>array('create','edit')));
	Route::resource('department','DepartmentController',array('except'=>array('create','edit')));
	Route::resource('masking','MaskingController',array('except'=>array('create','edit')));
	Route::resource('filehistory','FileHistoryController',array('except'=>array('create','edit')));
});

Route::auth();