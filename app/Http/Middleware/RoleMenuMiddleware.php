<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Menu;

class RoleMenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menu = Menu::with('role')->where('route',$request->route()->getName())->first();
        if(empty($menu)) {
          //return redirect('/');
		  //print_r($request->route()->getName());
        }
		  //print_r($request->route()->getName());
        return $next($request);
    }
}
