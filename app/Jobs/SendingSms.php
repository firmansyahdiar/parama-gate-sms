<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Sms;

class SendingSms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $request;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
      $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
      ini_set('max_execution_time', 3600+1000);
      //API Url
      // $url = "http://lms.paramateknologi.local/api/sendings";
      $url = "http://smjkt-dvsms01/api/sendings";
      //echo $url;exit();
      //Initiate cURL.
      $ch = curl_init($url);

      //Encode the array into JSON.
      $jsonDataEncoded = json_encode($this->request);
	  $input['id']            = $this->request['id'];
	  $input['sending_status']= 12;
	  $this->save_sms($input);
	  
      //Tell cURL that we want to send a POST request.
      curl_setopt($ch, CURLOPT_POST, 1);

      //Attach our encoded JSON string to the POST fields.
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

      //Set the content type to application/json
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));

      //Execute the request
      $result = curl_exec($ch);
    }
	function save_sms($input)
    {
      if(!empty($input['id']))
      {
        $data = Sms::findOrFail($input['id']);
        $data->fill($input)->save();
      }
      else
      {
        Sms::create($input);
      }
    }
}
