<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','username', 'email', 'password','api_token', 'profile', 'role_id', 'department_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', //'remember_token','api_token'
    ];
    protected function getDateFormat()
      {
        return 'Y-m-d H:i:s';
      }
	public static $rules = [
		'username'     => 'required|alpha_spaces',
		'masking_id'   => 'required',
	];
}
